$(document).ready(function() {

	$("#addNewSsmInventory").click(function() {
		$('#inv_id').val("");
		$('#district_id').val("").change();
		$('#category_id').val("").change();
		$('#stock_cnt').val("");
		$('#activityType').val("I");
		var districtId = fetchDistrict();
		dropDownListOfDistrict(districtId);
		dropDownListOfCategory("");

		$('#ModalSsmInventory').modal('show');
	});

	$('#paginatedTable').DataTable({
		"processing": true,
		"serverSide": true,
		"pageLength": 5,
		"ajax": {
			"url": "/ssms/ssminventory/ssmInventoryList/paginated",
			"type": "GET",
			"data": function(data) {
				//process data before sent to server.
			}
		},
		"columns": [
			{ "data": "inv_id", "defaultContent": "" },
			{ "data": "district_id", "defaultContent": "" },
			{ "data": "district_name", "defaultContent": "" },
			{ "data": "category_id", "defaultContent": "" },
			{ "data": "category_name", "defaultContent": "" },
			{ "data": "stock_cnt", "defaultContent": "" },
			{
				"data": null,
				render: function(data, type, full, meta, row) {
					return type == 'display' ? '<button type="button" class="btn btn-outline-info btn-sm" style="" id = "editSsmInventory" onclick="editSsmInventory(\'' + full['inv_id'] + "','" + full['district_id'] + "','" + full['category_id'] + "','" + full['stock_cnt'] + '\')"><i class="fa fa-edit"</i></button>' : data
				}
			},
		]
	});

	$('#paginatedTable').dataTable().fnSetFilteringEnterPress();


});

function editSsmInventory(inv_id, district_id, category_id, stock_cnt) {
	//$("#district_id").children().remove();
	dropDownListOfDistrict(district_id);
	dropDownListOfCategory(category_id);
	$('#ModalSsmInventory').modal('show');
	$('#activityType').val("U");
	$('#inv_id').val(inv_id);
	/*$('#district_id').val(district_id).attr("defaultSelected","true");
	$('#category_id').val(category_id).change();*/
	$('#stock_cnt').val(stock_cnt);
	
}

function dropDownListOfDistrict(districtId)
{
	$.ajax({
                type: "POST",
                contentType: "application/json",
                data: "{}",
                url: "/ssms/ssminventory/ssmInventoryDistrictList",
                dataType: "json",
                success: function(data) {
                    var district_id = $("#district_id");
                    district_id.children().remove().end();
                    console.log(data.districtList);
                     district_id.append($("<option>").val("").text("-- Select --"));
                        $(data.districtList).each(function(index, item) {
							//console
                            district_id.append($("<option></option>").val(item.listKey).text(item.listValue));
                        });
                        district_id.val(districtId);
                },
            });
}


function dropDownListOfCategory(categoryId)
{
	$.ajax({
                type: "POST",
                contentType: "application/json",
                data: "{}",
                url: "/ssms/ssminventory/ssmInventoryCategoryList",
                dataType: "json",
                success: function(data) {
                    var category_id = $("#category_id");
                    category_id.find('option').remove().end();
                  // document.getElementById("category_id").options.length = 0;
                    console.log(data.categoryList);
                     category_id.append($("<option>").val("").text("-- Select --"));
                        $(data.categoryList).each(function(index, item) {
							//console
                            category_id.append($("<option></option>").val(item.listKey).text(item.listValue));
                        });
                        
                        category_id.val(categoryId);
                },
            });
}


function fetchDistrict()
{
	var districtId;
	$.ajax({
                type: "POST",
                contentType: "application/json",
                data: "{}",
                url: "/ssms/ssmorder/ssmFetchDistrictId",
                dataType: "json",
                async: false,
                success: function(data) {
                   
                   if(data.status=='ok')
                   {
						districtId =  data.districtId;
				   }
				   
                },
            });
            
        return districtId;
}
