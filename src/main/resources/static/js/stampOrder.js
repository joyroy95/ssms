$(document).ready(function() {

	$("#addNewSsmOrder").click(function() {
		$('#inv_id').val("");
		$('#district_id').val("").change();
		$('#category_id').val("").change();
		$('#stock_cnt').val("");
		$('#activityType').val("I");
		
		var districtId = fetchDistrict();
		dropDownListOfDistrict(districtId);
		dropDownListOfCategory("");
		
		var categoryId = $('#category_id').val();
		dropDownListOfInventory();

		$('#ModalSsmInventory').modal('show');
	});

	$('#paginatedTable').DataTable({
		"processing": true,
		"serverSide": true,
		"pageLength": 5,
		"ajax": {
			"url": "/ssms/ssmorder/stampOrderIndividualList/paginated",
			"data": function(data) {
				//process data before sent to server.
			}
		},
		"columns": [
			{ "data": "sal_id", "defaultContent": "" },
			{ "data": "district_name", "defaultContent": "" },
			{ "data": "category_name", "defaultContent": "" },
			{ "data": "inv_id", "defaultContent": "" },
			{ "data": "sale_amt", "defaultContent": "" },
			{ "data": "sale_fee", "defaultContent": "" },
			{ "data": "sale_total_amt", "defaultContent": "" },
			{ "data": "order_status", "defaultContent": "" },
		]
	});

	$('#paginatedTable').dataTable().fnSetFilteringEnterPress();
	
	pendingStampOrderDatatable();


});


function pendingStampOrderDatatable()
{
	$('#paginatedTableForPendingOrder').DataTable({
		"processing": true,
		"serverSide": true,
		"pageLength": 5,
		"ajax": {
			"url": "/ssms/ssmorder/stampOrderPendingList/paginated",
			"data": function(data) {
				//process data before sent to server.
			}
		},
		"columns": [
			{ "data": "sal_id", "defaultContent": "" },
			{ "data": "district_id", "defaultContent": "" },
			{ "data": "district_name", "defaultContent": "" },
			{ "data": "category_id", "defaultContent": "" },
			{ "data": "category_name", "defaultContent": "" },
			{ "data": "inv_id", "defaultContent": "" },
			{ "data": "sale_amt", "defaultContent": "" },
			{ "data": "order_by", "defaultContent": "" },
			{ "data": "order_date", "defaultContent": "" },
			{
				"data": null,
				render: function(data, type, full, meta, row) {
					return type == 'display' ? '<button type="button" class="btn btn-outline-info btn-sm" style="" id = "editSsmPendingOrder" onclick="editSsmPendingOrder(\'' + full['sal_id'] +  "','" + "A" +'\')"><i class="fa fa-check"</i></button>' : data
				}
			},
			{
				"data": null,
				render: function(data, type, full, meta, row) {
					return type == 'display' ? '<button type="button" class="btn btn-outline-info btn-sm" style="" id = "orderAutherize" onclick="orderAutherize(\'' + full['sal_id'] + "','" + "D" +'\')"><i class="fa fa-trash"</i></button>' : data
				}
			},
		],
		"bDestroy": true
	});

	$('#paginatedTableForPendingOrder').dataTable().fnSetFilteringEnterPress();
}

function editSsmPendingOrder(sal_id)
	{
		$('#sal_id').val(sal_id);
		$('#ModalSsmPendingOrder').modal('show');
	} 


function orderAutherize(sal_id, order_status) {
	
	console.log(order_status);
	
	 var json = {
       "sal_id" : sal_id,
       "order_status":order_status
      };
	
		$.ajax({
                contentType: "application/json",
                data: JSON.stringify(json),
                url: "/ssms/ssmorder/ssmPendingOrderAuthorize",
                dataType: "json",
                success: function(data) {
                   
                    if(data.status=='ok')
                    {
						$("#suc").empty();
						var message = data.message;
						$("#suc").append(" <h6>"+message+"</h6>.");
						$( "div.success" ).fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
					}
					else
					{
						$("#fal").empty();
						var message = data.message;
						$("#fal").append(" <h6>"+message+"</h6>.");
						$( "div.failure" ).fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
					}
                    
                },
            });
            
            
            pendingStampOrderDatatable();
	
}


function dropDownListOfDistrict(districtId)
{
	$.ajax({
                type: "POST",
                contentType: "application/json",
                data: "{}",
                url: "/ssms/ssminventory/ssmInventoryDistrictList",
                dataType: "json",
                success: function(data) {
                    var district_id = $("#district_id");
                    district_id.children().remove().end();
                    console.log(data.districtList);
                     district_id.append($("<option>").val("").text("-- Select --"));
                        $(data.districtList).each(function(index, item) {
							//console
                            district_id.append($("<option></option>").val(item.listKey).text(item.listValue));
                        });
                        district_id.val(districtId);
                },
            });
}


function dropDownListOfCategory(categoryId)
{
	$.ajax({
                type: "POST",
                contentType: "application/json",
                data: "{}",
                url: "/ssms/ssminventory/ssmInventoryCategoryList",
                dataType: "json",
                success: function(data) {
                    var category_id = $("#category_id");
                    category_id.find('option').remove().end();
                  // document.getElementById("category_id").options.length = 0;
                    console.log(data.categoryList);
                     category_id.append($("<option>").val("").text("-- Select --"));
                        $(data.categoryList).each(function(index, item) {
							//console
                            category_id.append($("<option></option>").val(item.listKey).text(item.listValue));
                        });
                        
                        category_id.val(categoryId);
                },
            });
}



function dropDownListOfInventory()
{
	$.ajax({
                type: "POST",
                contentType: "application/json",
                data: "{}",
                url: "/ssms/ssmorder/ssmInventoryList",
                dataType: "json",
                success: function(data) {
                    var inv_id = $("#inv_id");
                    inv_id.find('option').remove().end();
                  // document.getElementById("category_id").options.length = 0;
                    console.log(data.inventoryList);
                     inv_id.append($("<option>").val("").text("-- Select --"));
                        $(data.inventoryList).each(function(index, item) {
							//console
                            inv_id.append($("<option></option>").val(item.listKey).text(item.listValue));
                        });
                        
                        /*inv_id.val(categoryId);*/
                },
            });
}




function fetchDistrict()
{
	var districtId;
	$.ajax({
                type: "POST",
                contentType: "application/json",
                data: "{}",
                url: "/ssms/ssmorder/ssmFetchDistrictId",
                dataType: "json",
                async: false,
                success: function(data) {
                   
                   if(data.status=='ok')
                   {
						districtId =  data.districtId;
				   }
				   
                },
            });
            
        return districtId;
}



function fetchInvId(CategoryId, districtId) {

	var json = {
		"category_id": CategoryId,
		"district_id": districtId,
	};

	$.ajax({
		type: "POST",
		contentType: "application/json",
		data: JSON.stringify(json),
		url: "/ssms/ssmorder/ssmFetchInvIdByCate",
		dataType: "json",
		success: function(data) {

			if (data.status == 'ok') {
				$("#inv_id").val(data.invId);
			}
			else {
				$("#inv_id").val("");
			}

		},
	});

}

$("#category_id").change(function(){  
	var districtId = $("#district_id").val();
	var categoryId = $(this).val()
    fetchInvId(categoryId, districtId);
});


$("#sale_fee").change(function(){  
	var stockCount = $("#sale_amt").val();
	var saleFee = $(this).val()
    
    var saleTotalAmount = saleFee*stockCount;
    
    $("#sale_total_amt").val(saleTotalAmount);
    
});





