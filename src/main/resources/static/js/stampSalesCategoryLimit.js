$(document).ready(function() {

	$("#addNewCategoryLimit").click(function() {
		$('#district_id1').val("").change();
		$('#category_id1').val("").change();
		$('#limit_cnt').val("");
		$('#activityType').val("I");
		var districtId = fetchDistrict();
		dropDownListOfDistrict(districtId);
		$("#district_id").val(districtId);
		dropDownListOfCategory("");
		
		$('#category_id1').prop("disabled", false);
	    $('#district_id1').prop("disabled", false);

		$('#ModalSsmCategoryLimit').modal('show');
	});

	$('#paginatedTableForCategoryLimit').DataTable({
		"processing": true,
		"serverSide": true,
		"pageLength": 5,
		"ajax": {
			"url": "/ssms/ssmcategorylmt/ssmCategoryLimitList/paginated",
			"type": "GET",
			"data": function(data) {
				//process data before sent to server.
			}
		},
		"columns": [
			{ "data": "district_id", "defaultContent": "" },
			{ "data": "district_name", "defaultContent": "" },
			{ "data": "category_id", "defaultContent": "" },
			{ "data": "category_name", "defaultContent": "" },
			{ "data": "limit_cnt", "defaultContent": "" },
			{
				"data": null,
				render: function(data, type, full, meta, row) {
					return type == 'display' ? '<button type="button" class="btn btn-outline-info btn-sm" style="" id = "editSsmCategoryLimit" onclick="editSsmCategoryLimit(\'' + full['district_id'] + "','" + full['category_id'] + "','" + full['limit_cnt'] + '\')"><i class="fa fa-edit"</i></button>' : data
				}
			},
		]
	});

	$('#paginatedTableForCategoryLimit').dataTable().fnSetFilteringEnterPress();


});

function editSsmCategoryLimit(district_id, category_id, limit_cnt) {
	//$("#district_id").children().remove();
	dropDownListOfDistrict(district_id);
	dropDownListOfCategory(category_id);
	$("#category_id").val(category_id);
	$("#district_id").val(district_id);
	$('#ModalSsmCategoryLimit').modal('show');
	$('#activityType').val("U");
	$('#category_id1').prop("disabled", true);
	$('#district_id1').prop("disabled", true);
	$('#limit_cnt').val(limit_cnt);
	
}

function dropDownListOfDistrict(districtId)
{
	$.ajax({
                type: "POST",
                contentType: "application/json",
                data: "{}",
                url: "/ssms/ssminventory/ssmInventoryDistrictList",
                dataType: "json",
                success: function(data) {
                    var district_id = $("#district_id1");
                    district_id.children().remove().end();
                    console.log(data.districtList);
                     district_id.append($("<option>").val("").text("-- Select --"));
                        $(data.districtList).each(function(index, item) {
							//console
                            district_id.append($("<option></option>").val(item.listKey).text(item.listValue));
                        });
                        district_id.val(districtId);
                },
            });
}


function dropDownListOfCategory(categoryId)
{
	$.ajax({
                type: "POST",
                contentType: "application/json",
                data: "{}",
                url: "/ssms/ssminventory/ssmInventoryCategoryList",
                dataType: "json",
                success: function(data) {
                    var category_id = $("#category_id1");
                    category_id.find('option').remove().end();
                  // document.getElementById("category_id").options.length = 0;
                    console.log(data.categoryList);
                     category_id.append($("<option>").val("").text("-- Select --"));
                        $(data.categoryList).each(function(index, item) {
							//console
                            category_id.append($("<option></option>").val(item.listKey).text(item.listValue));
                        });
                        
                        category_id.val(categoryId);
                },
            });
}


function fetchDistrict()
{
	var districtId;
	$.ajax({
                type: "POST",
                contentType: "application/json",
                data: "{}",
                url: "/ssms/ssmorder/ssmFetchDistrictId",
                dataType: "json",
                async: false,
                success: function(data) {
                   
                   if(data.status=='ok')
                   {
						districtId =  data.districtId;
				   }
				   
                },
            });
            
        return districtId;
}

$("#category_id1").change(function(){  
	var categoryId = $(this).val();
	$("#category_id").val(categoryId);
});

$("#district_id1").change(function(){  
	var districtId = $(this).val();
	$("#district_id").val(districtId);
});
