$(document).ready(function() {
	
	$( '.datepicker' ).datepicker({ dateFormat: 'dd/mm/yy' });
	
	$('#paginatedTableForStampOrderReportForOfficial').DataTable({});
	$('#paginatedTableForStampOrderReportForCustomer').DataTable({});
	
	$("#dateWiseSearchReportForCustomer").click(function(){
		
		var from_date = $('#from_date').val();
		var to_date =  $('#to_date').val();
		
  		stampOrderCustomerReportDatatable(from_date, to_date);
	});
	
	$("#dateWiseSearchReportForOfficial").click(function(){
		
		var from_date = $('#from_date').val();
		var to_date =  $('#to_date').val();
		
  		stampOrderOfficialReportDatatable(from_date, to_date);
	});


});

function stampOrderCustomerReportDatatable(from_date, to_date)
{
		$('#paginatedTableForStampOrderReportForCustomer').DataTable({
		"processing": true,
		"serverSide": true,
		"pageLength": 5,
		"ajax": {
			"type": "POST",
			"url": "/ssms/ssmOrderReport/stampOrderIndividualReportList/paginated",
			"data": {
				from_date : from_date,
				to_date:to_date
			}
		},
		"columns": [
			{ "data": "sl_no", "defaultContent": "" },
			{ "data": "district_name", "defaultContent": "" },
			{ "data": "category_name", "defaultContent": "" },
			{ "data": "sale_amt", "defaultContent": "" },
			{ "data": "sale_fee", "defaultContent": "" },
			{ "data": "sale_total_amt", "defaultContent": "" },
			{ "data": "chalan_no", "defaultContent": "N/A" },
			{ "data": "stamp_receive_date", "defaultContent": "N/A" },
			{ "data": "order_by", "defaultContent": "" },
			{ "data": "order_date", "defaultContent": "" },
			{ "data": "sale_by", "defaultContent": "" },
			{ "data": "sale_date", "defaultContent": "" },
			{ "data": "order_status", "defaultContent": "" },
		],
		"bDestroy": true,
		"dom": "<'row'<'col-sm-3'l><'col-sm-6 text-center'B><'col-sm-3'f>>" + "<'row'<'col-sm-12'tr>>" +"<'row'<'col-sm-5'i><'col-sm-7'p>>",
		
		"buttons": [
			'copyHtml5',
			'excelHtml5',
			'csvHtml5',
			'pdfHtml5'
		],
		"lengthMenu": [[5,10, 50, 200,500, 1000], [5,10, 50, 200,500, 1000]]
	});

	$('#paginatedTableForStampOrderReportForCustomer').dataTable().fnSetFilteringEnterPress();

}


function stampOrderOfficialReportDatatable(from_date, to_date)
{
	console.log(from_date);
	console.log(to_date);
	var json = {
       "from_date" : from_date,
       "to_date":to_date
      };
	$('#paginatedTableForStampOrderReportForOfficial').DataTable({
		"processing": true,
		"serverSide": true,
		"pageLength": 5,
		"ajax": {
			"type": "POST",
			"url": "/ssms/ssmOrderReport/stampOrderOfficialStampOrderReportList/paginated",
			"data": {
				from_date : from_date,
				to_date:to_date
			}
		},
		
		"columns": [
			{ "data": "sl_no", "defaultContent": "" },
			{ "data": "district_name", "defaultContent": "" },
			{ "data": "category_name", "defaultContent": "" },
			{ "data": "sale_amt", "defaultContent": "" },
			{ "data": "sale_fee", "defaultContent": "" },
			{ "data": "sale_total_amt", "defaultContent": "" },
			{ "data": "chalan_no", "defaultContent": "N/A" },
			{ "data": "stamp_receive_date", "defaultContent": "N/A" },
			{ "data": "order_by", "defaultContent": "" },
			{ "data": "order_date", "defaultContent": "" },
			{ "data": "sale_by", "defaultContent": "" },
			{ "data": "sale_date", "defaultContent": "" },
			{ "data": "order_status", "defaultContent": "" },
		],
		"bDestroy": true,
		"dom": "<'row'<'col-sm-3'l><'col-sm-6 text-center'B><'col-sm-3'f>>" + "<'row'<'col-sm-12'tr>>" +"<'row'<'col-sm-5'i><'col-sm-7'p>>",
		
		"buttons": [
			'copyHtml5',
			'excelHtml5',
			'csvHtml5',
			'pdfHtml5'
		],
		"lengthMenu": [[5,10, 50, 200,500, 1000], [5,10, 50, 200,500, 1000]]	});

	$('#paginatedTableForStampOrderReportForOfficial').dataTable().fnSetFilteringEnterPress();
}

