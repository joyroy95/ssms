$(document ).ready(function() {
	
	$("#addNewSsmCategory").click(function () {
				$('#category_id').prop("readonly", false);
				$('#activityType').val("");
				$('#category_id').val("");
				$('#category_name').val("");
				$('#category_description').val("");
				
				$('#ModalSsmCategory').modal('show');
			});
	
	$('#paginatedTable').DataTable( {
        "processing": true,
        "serverSide": true,
        "pageLength": 5,
        "ajax": {
            "url": "/ssms/ssmcategory/ssmCategoryList/paginated",
            "data": function ( data ) {
			 //process data before sent to server.
         }},
        "columns": [
                    { "data": "category_id" , "defaultContent":""},
					{ "data": "category_name" , "defaultContent":""},
					{ "data": "category_description" , "defaultContent":""},
					{ "data": null , 
						 render : function( data, type, full, meta, row  ) {
							console.log(full['category_id']);
      				 return type == 'display' ? '<button type="button" class="btn btn-outline-info btn-sm" style="" id = "editSsmCategory" onclick="editSsmCategory(\'' + full['category_id'] + "','" +full['category_name'] + "','" + full['category_description']+ '\')"><i class="fa fa-edit"</i></button>': data
     }
					},
                ]    
	});
	
	$('#paginatedTable').dataTable().fnSetFilteringEnterPress();
	
	
});





function editSsmCategory(category_id, category_name, category_description)
	{
		$('#activityType').val("U");
		$('#category_id').val(category_id);
		$('#category_id').prop("readonly", true);
		$('#category_name').val(category_name);
		$('#category_description').val(category_description);
		$('#ModalSsmCategory').modal('show');
	} 
	