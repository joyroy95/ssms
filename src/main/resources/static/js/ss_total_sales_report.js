$(document).ready(function() {
	
  		stampTotalSalesReportDatatable();


});



function stampTotalSalesReportDatatable()
{
	$('#paginatedTableForStampTotalSales').DataTable({
		"processing": true,
		"serverSide": true,
		"pageLength": 5,
		"ajax": {
			"url": "/ssms/ssmTotalSales/ssmCategoryWiseTotalSalesReportList/paginated",
			"data": function ( data ) {
			 //process data before sent to server.
         }
		},
		
		"columns": [
			{ "data": "sl_no", "defaultContent": "" },
			{ "data": "category_name", "defaultContent": "" },
			{ "data": "total_sale", "defaultContent": "" },
		],
		"bDestroy": true,
		"dom": "<'row'<'col-sm-3'l><'col-sm-6 text-center'B><'col-sm-3'f>>" + "<'row'<'col-sm-12'tr>>" +"<'row'<'col-sm-5'i><'col-sm-7'p>>",
		
		"buttons": [
			'copyHtml5',
			'excelHtml5',
			'csvHtml5',
			'pdfHtml5'
		],
		"lengthMenu": [[5,10, 25, 50, 100, 1000], [5,10, 25, 50, 100, 1000]]	});

	$('#paginatedTableForStampTotalSales').dataTable().fnSetFilteringEnterPress();
}

