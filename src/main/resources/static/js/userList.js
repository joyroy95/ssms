$(document).ready(function() {
	
	$("#addNewUser").click(function() {
		$('#userId').prop("readonly", false);
		$('#activityType').val('I');
		 $('.ssmUserDiv').find('input:text').val('');
		dropDownListOfDistrict("");
		$('#ModalUserManagement').modal('show');
		
	});

	userListDatatable();


});

function dropDownListOfDistrict(districtId)
{
	$.ajax({
                type: "POST",
                contentType: "application/json",
                data: "{}",
                url: "/ssms/ssminventory/ssmInventoryDistrictList",
                dataType: "json",
                success: function(data) {
                    var district_id = $("#userSellingArea");
                    district_id.children().remove().end();
                    console.log(data.districtList);
                     district_id.append($("<option>").val("").text("-- Select --"));
                        $(data.districtList).each(function(index, item) {
							//console
                            district_id.append($("<option></option>").val(item.listKey).text(item.listValue));
                        });
                        district_id.val(districtId);
                },
            });
}


function userListDatatable() {
	$('#paginatedTableForUserList').DataTable({
		"processing": true,
		"serverSide": true,
		"pageLength": 5,
		"ajax": {
			"url": "/ssms/admin/ssmUserMasterList/paginated",
			"data": function(data) {
				//process data before sent to server.
			}
		},
		"columns": [
			{
				"data": null,
				render: function(data, type, full, meta, row) {
					return type == 'display' ? '<button type="button" class="btn btn-outline-info btn-sm" style="" id = "editUserMaster" onclick="editUserInfo(\'' + full['user_id'] + '\')"><i class="fa fa-edit"</i></button>' : data

				}
			},
			{
				"data": null,
				render: function(data, type, full, meta, row) {
					if (full['auth_status'] == 'U') {
						return type == 'display' ? '<button type="button" class="btn btn-outline-info btn-sm" style="" id = "userAutherize" onclick="userAutherize(\'' + full['user_id'] + '\')"><i class="fa fa-check"</i></button>' : data
					}
					else {
						return type == 'display' ? "Authorize" : data
					}

				}
			},
			{ "data": "user_id", "defaultContent": "" },
			{ "data": "user_name", "defaultContent": "" },
			{ "data": "district_name", "defaultContent": "" },
			{ "data": "user_status", "defaultContent": "" },
			
			
		],
		
		"bDestroy": true,
		"lengthMenu": [[5,10, 25, 50, 100], [5,10, 25, 50, 100]]
	});

	$('#paginatedTableForUserList').dataTable().fnSetFilteringEnterPress();
}


function userAutherize(user_id) {

	var json = {
		"userId": user_id
	};

	$.ajax({
		type: "POST",
		contentType: "application/json",
		data: JSON.stringify(json),
		url: "/ssms/admin/ssmUserAuthorize",
		dataType: "json",
		success: function(data) {

			if (data.status == 'ok') {
				$("#suc").empty();
				var message = data.message;
				$("#suc").append(" <h6>" + message + "</h6>.");
				$("div.success").fadeIn(300).delay(1500).fadeOut(400);
			}
			else {
				$("#fal").empty();
				var message = data.message;
				$("#fal").append(" <h6>" + message + "</h6>.");
				$("div.failure").fadeIn(300).delay(1500).fadeOut(400);
			}

		},
	});


	userListDatatable();

}



function editUserInfo(userId) {

		var json = {
		"userId": userId
	};

	$.ajax({
		type: "POST",
		contentType: "application/json",
		data: JSON.stringify(json),
		url: "/ssms/admin/ssmUserInfoEdit",
		dataType: "json",
		success: function(data) {
			$('#activityType').val('U');
			$('#userId').prop("readonly", true);
			$('#userId').val(data.userInfo.userId);
			$('#userName').val(data.userInfo.userName);
			$('#userMobile').val(data.userInfo.userMobile);
			$('#userEmail').val(data.userInfo.userEmail);
			$('#userType').val(data.userInfo.userType);
			$('#userFathersName').val(data.userInfo.userFathersName);
			$('#userMothersName').val(data.userInfo.userMothersName);
			$('#userNid').val(data.userInfo.userNid);
			var districtId = data.userInfo.userSellingArea;
			dropDownListOfDistrict(districtId);
			//$('#userSellingArea').val(districtId);
			$('#userLicenseNo').val(data.userInfo.userLicenseNo);
			var fetchDate = data.userInfo.userLicenseIssueDate;
			$('#userLicenseIssueDate').val(dateFormat(fetchDate));
			$('#userBankSolvency').val(data.userInfo.userBankSolvency);
			$('#userPermanentAddress').val(data.userInfo.userPermanentAddress);
			$('#userPresentAddress').val(data.userInfo.userPresentAddress);
			$('#userStatus').val(data.userInfo.userStatus);
			
		},
	});
		
		$('#ModalUserManagement').modal('show');

}


function dateFormat(dateObject) {
    var d = new Date(dateObject);
    var day = d.getDate();
    var month = d.getMonth() + 1;
    var year = d.getFullYear();
    if (day < 10) {
        day = "0" + day;
    }
    if (month < 10) {
        month = "0" + month;
    }
    var date = year + "-" + month + "-" + day;

    return date;
};

