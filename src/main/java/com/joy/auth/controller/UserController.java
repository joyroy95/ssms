package com.joy.auth.controller;

import java.security.Principal;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.gson.Gson;
import com.joy.auth.model.Customer;
import com.joy.auth.service.AppUserService;
import com.joy.auth.service.CustomerService;
import com.joy.auth.service.MenuMasterService;
import com.joy.auth.utils.WebUtils;
import com.joy.auth.common.ListVo;
import com.joy.auth.domain.EmployeesModel;
import com.joy.auth.domain.UserModel;
import com.joy.auth.pagination.DataTableRequest;
import com.joy.auth.pagination.DataTableResults;
import com.joy.auth.pagination.PaginationCriteria;
import com.joy.auth.utils.*;

@RestController
public class UserController {
	
	@Autowired
	CustomerService CustomerService;
	
	@Autowired
	 private AppUserService setpassword; 
	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	MenuMasterService menuMasterService;
	
	private String roleName;

	@GetMapping(value = "/userInfo")
	public ModelAndView userInfo(ModelAndView model, Principal principal) {
		String userName = principal.getName();

		System.out.println("User Name: " + userName);

		User loginedUser = (User) ((Authentication) principal).getPrincipal();

		String userInfo = WebUtils.toString(loginedUser);
		model.addObject("userInfo", userInfo);
		model.setViewName("userInfoPage");
		return model;
	}

	@GetMapping(value = "/userInfo/openSavingsAccount")
	public ModelAndView getSavingAcoount(ModelAndView model) {

		model.setViewName("openSavingsAccount");
		return model;
	}

	@PostMapping(value = "/userInfo/openSavingsAccount")
	public ModelAndView postSavingAcoount(ModelAndView model, Customer customer,
			@RequestParam("radio") String customer1, Authentication authentication) {

		customer.setAccountType(customer1);
		String message="";
		
		System.out.println("Branch code: " + customer.getBranchCode());
		try {
			CustomerService.insetIntoCustomer(customer, authentication.getName());
			String success = CustomerService.createAccount(customer);
			model.addObject("success", "Customer Account is succesfully created and Account Number is: "+ success);
		} catch (DuplicateKeyException e) {
			message+="Nid is already exists";
		}
		model.addObject("message", message);
		model.setViewName("openSavingsAccount");
		return model;
	}
	
	@RequestMapping(value = "/userInfo/changePassword", method = RequestMethod.GET)

    public ModelAndView ChangePassWordPage(ModelAndView model,Authentication authentication) {
		
		authentication.getAuthorities().forEach(s -> roleName = s.getAuthority());

		try {
			List<ListVo> menuList = menuMasterService.loadMenuListByRole(roleName);
			model.addObject("menuList", menuList);

			menuList.forEach(m -> System.out.println(m.getListKey() + " " + m.getListValue()));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
         
        model.addObject("user", authentication.getName());
        model.setViewName("changePasswordPage");
         
        return model;
    }
    
    @PostMapping(value = "/userInfo/changePassword")
    public ModelAndView ChangePassWordPage(ModelAndView model,Authentication authentication, String password, String new_password, RedirectAttributes atts) {
    	String message = "";
    	String success="";
    	if(setpassword.checkAdminCurrentPassword(authentication.getName(), password))
    	{
    		if(password.equals(new_password))
    		{
    			message += "Existing Password and New password can't be Same";
    		}
    		else
    		{
    			setpassword.updatePassword(authentication.getName(), new_password);
    			success += "Password is changed Successfully"; 
    			model.addObject("success", success);
    		}
    	}
    	else
    	{
    		message += "Existing Password is not Matched!!";
    	}
    	
    	atts.addFlashAttribute("message", message);
    	atts.addFlashAttribute("success", success);
		return new ModelAndView("redirect:/userInfo/changePassword");
    }
    
    @GetMapping(value = "/userInfo/depositMoney")
	public ModelAndView getDepositMoney(ModelAndView model) {
		
		model.setViewName("depositMoney");
		return model;
	}
    
    @PostMapping(value = "/userInfo/depositMoney")
   	public ModelAndView DepositMoney(ModelAndView model, String accountNo, int amount) {
    	String message="";
    	String success="";
    	try {
    		CustomerService.checkAccountNumber(accountNo);
    		CustomerService.insertIntoTransactionHistoryCredit(accountNo, amount);
    		success+= "Account Number: "+ accountNo +" credited "+ amount + " Taka Successfully!";
    		model.addObject("success", success);
		} catch (EmptyResultDataAccessException e) {
			System.out.println("Not found");
			message+="Account Number is not Exist";
		}
   		
    	System.out.println(accountNo + " " + amount);
    	model.addObject("message", message);
   		model.setViewName("depositMoney");
   		return model;
   	}
    
    @GetMapping(value = "/userInfo/withdrawMoney")
	public ModelAndView getWithdrawMoney(ModelAndView model) {
		
		model.setViewName("withdrawMoney");
		return model;
	}
    
    @PostMapping(value = "/userInfo/withdrawMoney")
   	public ModelAndView WithdrawMoney(ModelAndView model, String accountNo, int amount) {
    	String message="";
    	String success="";
    	try {
    		CustomerService.checkAccountNumber(accountNo);
    		if(CustomerService.insertIntoTransactionHistoryDebit(accountNo, amount)) {
    		success+= "Account Number: "+ accountNo +" debited "+ amount + " Taka Successfully!";
    		model.addObject("success", success);
    	}
    		else
    		{
    			message+="Account Number: "+ accountNo + "  hasn't Sufficient balance!";
    			model.addObject("message", message);
    		}
		} catch (EmptyResultDataAccessException e) {
			System.out.println("Not found");
			message+="Account Number is not Exist.";
		}
   		
    	System.out.println(accountNo + " " + amount);
    	model.addObject("message", message);
   		model.setViewName("withdrawMoney");
   		return model;
   	}
    
    @GetMapping(value = "/userInfo/customerInformation")
   	public ModelAndView getCustomerInformation(ModelAndView model) {
   		
   		model.setViewName("customerInformation");
   		return model;
   	}
    
    @PostMapping(value = "/userInfo/customerInformation")
   	public ModelAndView CustomerInformation(ModelAndView model, String accountNo) {
   		
    	Map<String, Object> map= (Map<String, Object>) CustomerService.showInformation(accountNo);
    	if(map.size()==0)
    	{
    		model.addObject("message", "Account Number doesn't exist.");
    	}
    	else
    	{
    	model.addObject("success", "Customer Information");
    	model.addObject("map", map);
    	}
    	System.out.println(map.get("ACCOUNT_NO"));;
   		model.setViewName("customerInformation");
   		return model;
   	}
}
