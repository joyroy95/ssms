package com.joy.auth.controller;

import java.io.IOException;
import java.net.UnknownHostException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.validation.constraints.Email;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.gson.Gson;
import com.joy.auth.common.ListVo;
import com.joy.auth.domain.StampOrderPendingModel;
import com.joy.auth.domain.UserMasterModel;
import com.joy.auth.pagination.DataTableRequest;
import com.joy.auth.pagination.DataTableResults;
import com.joy.auth.pagination.PaginationCriteria;
import com.joy.auth.service.AppUserService;
import com.joy.auth.service.EmployeeService;
import com.joy.auth.service.MenuMasterService;
import com.joy.auth.service.SsmInventoryService;
import com.joy.auth.utils.AppUtil;
import com.joy.auth.utils.EmailSend;
import com.joy.auth.utils.GeneratePassword;
import com.joy.auth.vo.StampOrderVo;
import com.joy.auth.vo.UserMasterVo;
import com.sun.mail.util.MailConnectException;

import javassist.NotFoundException;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

@RestController
public class AdminController {
	
	//private static final Logger log = LogManager.getLogger(AdminController.class);

	@Autowired
	EmployeeService employeeService;

	@Autowired
	AppUserService appUserService;

	@Autowired
	EmailSend emailSend;

	@Autowired
	SsmInventoryService ssmInventoryService;
	
	@Autowired
	MenuMasterService menuMasterService;
	
	private String roleName;

	@PersistenceContext
	private EntityManager entityManager;

	String generatedPassword;

	@GetMapping(value = "/admin/index")
	public ModelAndView getIndex(ModelAndView model) {
		model.setViewName("index");
		return model;
	}

	@GetMapping(value = "/admin/createNewUser")
	public ModelAndView getCreateUserPage(ModelAndView model, Authentication authentication) {
		//log.info("Entering into getCreateUserPage method in AdminController Class");
		try {
			List<ListVo> districtList = ssmInventoryService.loadDistrictListForDropDown();
			model.addObject("districtList", districtList);

		} catch (Exception e) {
			// TODO: handle exception
		}
		
		authentication.getAuthorities().forEach(s -> roleName = s.getAuthority());

		try {
			List<ListVo> menuList = menuMasterService.loadMenuListByRole(roleName);
			model.addObject("menuList", menuList);

			menuList.forEach(m -> System.out.println(m.getListKey() + " " + m.getListValue()));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		model.setViewName("SS_User/ssUserList");
		return model;
	}

	private static final String ROLES = "roles";

	@ModelAttribute
	public void initValues(Model model) {
		List<String> roles = appUserService.getRoles();
		model.addAttribute(ROLES, roles);
	}

	@GetMapping(value = "/admin/assignNewRoleToUser")

	public ModelAndView getAssignNewRoleToUser(ModelAndView model, Authentication authentication) {
		
		authentication.getAuthorities().forEach(s -> roleName = s.getAuthority());

		try {
			List<ListVo> menuList = menuMasterService.loadMenuListByRole(roleName);
			model.addObject("menuList", menuList);

			menuList.forEach(m -> System.out.println(m.getListKey() + " " + m.getListValue()));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("test: " + authentication.getName());

		model.setViewName("assignNewRoleToUser");
		return model;
	}

	@PostMapping(value = "/admin/assignNewRoleToUser")

	public ModelAndView assignNewRoleToUser(ModelAndView model, @RequestParam List<String> roles, String user_id,
			BindingResult bindingResult, RedirectAttributes atts) {
		
		String message = "";
		String success = "";
		try {
		
		model.setViewName("assignNewRoleToUser");
		
		if (appUserService.UserIdcheck(user_id)) {
			boolean check = true;
			for (String role : roles) {
				System.out.println(role);
				if (appUserService.assignRole(user_id, role)) {
					check = false;
				}
			}
			if (check) {
				message += "This Role already assigned to the user";

			} else {
				success += "User Id: " + user_id + " assign role successfully.";

			}

		} else {
			message += "User Id is not created yet.";
		}
		} catch (Exception e) {
			System.out.println(e.getCause());
		}

		atts.addFlashAttribute("message", message);
		atts.addFlashAttribute("success", success);
		return new ModelAndView("redirect:/admin/assignNewRoleToUser");

	}

	@PostMapping(value = "/admin/createNewUser")

	public ModelAndView createUserPage(ModelAndView model, UserMasterVo userMasterVo, RedirectAttributes atts) {
		String message = "";
		System.out.println("email from create user: " + userMasterVo.getUserEmail());

		
		String success = "";

		System.out.println("From createUserPage Method: " + userMasterVo);
		try {
			Map<String, String> spFeedBack = appUserService.executeUserMasterSaveSp(userMasterVo);

			if (spFeedBack.get("P_OUT").equals("0")) {
				
				if(userMasterVo.getActivityType().equals("I"))
				{
					
					success += "User " + userMasterVo.getUserId()+ " Created Succesfully.";
				}
				else
				{
					success += "User " + userMasterVo.getUserId()+ " Updated Succesfully.";
				}
				
			} else {
				message += spFeedBack.get("P_ERROR_MESSAGE");
			}
		} catch (Exception e) {
			message += e;
			System.out.println(e);
		}

		atts.addFlashAttribute("message", message);
		atts.addFlashAttribute("success", success);
		return new ModelAndView("redirect:/admin/ssmUserList");
	}

	@GetMapping(value = "/admin/resetPassword")

	public ModelAndView getResetPassword(ModelAndView model, Authentication authentication) {
		
		authentication.getAuthorities().forEach(s -> roleName = s.getAuthority());

		try {
			List<ListVo> menuList = menuMasterService.loadMenuListByRole(roleName);
			model.addObject("menuList", menuList);

			menuList.forEach(m -> System.out.println(m.getListKey() + " " + m.getListValue()));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		model.setViewName("resetPassword");
		return model;
	}

	@PostMapping(value = "/admin/resetPassword")
	public ModelAndView resetPassword(ModelAndView model, String userId, String email, RedirectAttributes atts) {
		String message = "";
		String success = "";
		String rawPassword = generatedPassword();
		BCryptPasswordEncoder Encoder = new BCryptPasswordEncoder();
		String encryptedPassword = Encoder.encode(rawPassword);
		if (appUserService.UserIdcheck(userId)) {

			appUserService.resetPassword(userId, encryptedPassword);
			try {

				emailSend.sendEmail(rawPassword, email, userId);
				System.out.println(rawPassword + "  " + encryptedPassword);
				success += "Password reset of User Id: " + userId + " succesfully"
						+ " and Default password sent to mail.";

			} catch (Exception e) {
				message += "Password reset Succesfully but Email can't sent for this reason." + e.getCause();

			}

		}

		else {
			message += "User Id is not created yet.";
			model.addObject("message", message);
		}
		atts.addFlashAttribute("message", message);
		atts.addFlashAttribute("success", success);
		return new ModelAndView("redirect:/admin/resetPassword");
	}

	public static String generatedPassword() {

		String randomPassword = GeneratePassword.generateRandomPassword(10);
		return randomPassword;

	}

	@RequestMapping(value = "/admin/ssmUserList", method = RequestMethod.GET)
	public ModelAndView landOnDisplayInventoryList(ModelAndView model, Authentication authentication) {
		
		authentication.getAuthorities().forEach(s -> roleName = s.getAuthority());

		try {
			List<ListVo> menuList = menuMasterService.loadMenuListByRole(roleName);
			model.addObject("menuList", menuList);

			menuList.forEach(m -> System.out.println(m.getListKey() + " " + m.getListValue()));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		model.setViewName("SS_User/ssUserList.html");
		return model;
	}

	/**
	 **** This method is used to paginated ssm user list.
	 **** 
	 * @author Joy Roy <joyroylightoj@gmail.com>
	 **/
	
	@RequestMapping(value = "/admin/ssmUserMasterList/paginated", method = RequestMethod.GET)
	@ResponseBody
	public String listUsersPaginatedForPendingStampOrder(HttpServletRequest request, HttpServletResponse response,
			Model model, Authentication authentication) {

		DataTableRequest<User> dataTableInRQ = new DataTableRequest<User>(request);
		PaginationCriteria pagination = dataTableInRQ.getPaginationRequest();
		String baseQuery = "SELECT u.USER_ID,\r\n" + "       u.USER_NAME,\r\n" + "       (SELECT d.DISTRICT_NAME\r\n"
				+ "          FROM ssm_district d\r\n"
				+ "         WHERE d.DISTRICT_ID = u.USER_SELLING_AREA)    DISTRICT_NAME,\r\n"
				+ "       Case When u.USER_STATUS='A'\r\n" + "       THEN 'Active'\r\n"
				+ "       When u.USER_STATUS='L'\r\n" + "       THEN 'Locked'\r\n" + "       When u.USER_STATUS='D'\r\n"
				+ "       THEN 'Disabled'\r\n" + "       WHEN u.USER_STATUS='C'\r\n" + "       THEN 'CLOSE'\r\n"
				+ "       END USER_STATUS,\r\n" + "       u.AUTH_STATUS\r\n" + "  FROM user_master u";
		System.out.println(baseQuery);
		String paginatedQuery = AppUtil.buildPaginatedQueryForOracle(baseQuery, pagination);

		Query query = entityManager.createNativeQuery(paginatedQuery, UserMasterModel.class);

		@SuppressWarnings("unchecked")
		List<UserMasterModel> userList = query.getResultList();

		DataTableResults<UserMasterModel> dataTableResult = new DataTableResults<UserMasterModel>();
		dataTableResult.setDraw(dataTableInRQ.getDraw());
		dataTableResult.setListOfDataObjects(userList);
		if (!AppUtil.isObjectEmpty(userList)) {
			dataTableResult.setRecordsTotal(userList.get(0).getTotalRecords().toString());
			if (dataTableInRQ.getPaginationRequest().isFilterByEmpty()) {
				dataTableResult.setRecordsFiltered(userList.get(0).getTotalRecords().toString());
			} else {
				dataTableResult.setRecordsFiltered(Integer.toString(userList.size()));
			}
		}
		return new Gson().toJson(dataTableResult);
	}

	@RequestMapping(value = "/admin/ssmUserAuthorize", method = RequestMethod.POST)
	@ResponseBody
	public String authorizeSsmUser(HttpServletResponse response, Model model,
			@RequestBody UserMasterVo userMasterVo, Authentication authentication) {

		Map<String, Object> jsonData = new HashMap<>();
		
		String rawPassword = generatedPassword();
		BCryptPasswordEncoder Encoder = new BCryptPasswordEncoder();
		String encryptedPassword = Encoder.encode(rawPassword);
		userMasterVo.setUserPassword(encryptedPassword);
		try {
			String userId = userMasterVo.getUserId();
			String checkerId = authentication.getName();
			
			String message = "";
			
				int row = appUserService.ssmUserMasterAuthorize(userMasterVo,userId, checkerId);
				
				

				if (row > 0) {
					message = "User " +userId+" Authorize Successfully.";
					
					userMasterVo = appUserService.loadUserEmailByUserId(userId);
					
					emailSend.sendEmail(rawPassword, userMasterVo.getUserEmail(), userId);
					message+="Password is sent to mail and Please Check Spam folder";

					jsonData.put("status", "ok");
				}
				
				else
				{
					message = "User " +userId+" Authorize Failure.";
				}

			jsonData.put("message", message);

		} catch (Exception e) {
			
			System.out.println(e.getCause());
			jsonData.put("status", "");
			jsonData.put("message", "");
			
		}
		return new Gson().toJson(jsonData);
	}
	
	

	@RequestMapping(value = "/admin/ssmUserInfoEdit", method = RequestMethod.POST)
	@ResponseBody
	public String editSsmUserInfo(HttpServletResponse response, Model model,
			@RequestBody UserMasterVo userMasterVo, Authentication authentication) {

		Map<String, Object> jsonData = new HashMap<>();
		
		try {
			String userId = userMasterVo.getUserId();
			
			String message = "";
			
				userMasterVo = appUserService.getAllUserInfoByUserId(userId);
				
				//userMasterVo.setUserLicenseIssueDate(userMasterVo.getUserLicenseIssueDate().\);
				System.out.println("userMasterVo: "  + userMasterVo);
				
				if(userMasterVo.getUserMobile().length()>0)
				{
					jsonData.put("userInfo", userMasterVo);
					message+="Fetch Successful";
				}

			jsonData.put("message", message);

		} catch (Exception e) {
			
			System.out.println(e.getCause());
			jsonData.put("status", "");
			jsonData.put("message", "");
			
		}
		return new Gson().toJson(jsonData);
	}

}
