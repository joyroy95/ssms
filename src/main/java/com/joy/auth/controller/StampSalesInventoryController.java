package com.joy.auth.controller;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.internal.build.AllowSysOut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.gson.Gson;
import com.joy.auth.common.ListVo;
import com.joy.auth.domain.SsmCategoryModel;
import com.joy.auth.domain.SsmInventoryModel;
import com.joy.auth.pagination.DataTableRequest;
import com.joy.auth.pagination.DataTableResults;
import com.joy.auth.pagination.PaginationCriteria;
import com.joy.auth.service.AppUserService;
import com.joy.auth.service.MenuMasterService;
import com.joy.auth.service.SsmInventoryService;
import com.joy.auth.utils.AppUtil;
import com.joy.auth.vo.SsmCategoryVo;
import com.joy.auth.vo.SsmInventoryVo;
import com.joy.auth.vo.UserMasterVo;

@RestController
@RequestMapping("/ssminventory")
public class StampSalesInventoryController {

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	SsmInventoryService ssmInventoryService;

	@Autowired
	MenuMasterService menuMasterService;
	
	@Autowired
	AppUserService appUserService;

	private String roleName;

	@RequestMapping(value = "/ssmInventoryList", method = RequestMethod.GET)
	public ModelAndView landOnDisplayInventoryList(ModelAndView model, Authentication authentication) {

		authentication.getAuthorities().forEach(s -> roleName = s.getAuthority());

		try {
			List<ListVo> menuList = menuMasterService.loadMenuListByRole(roleName);
			model.addObject("menuList", menuList);

			menuList.forEach(m -> System.out.println(m.getListKey() + " " + m.getListValue()));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		model.setViewName("SS_Inventory/stampSalesInventory-list");
		UserDetails userDetails = (UserDetails) authentication.getPrincipal();
		System.out.println("User has authorities: " + userDetails.getAuthorities());
		return model;
	}

	@RequestMapping(value = "/ssmInventoryDistrictList", method = RequestMethod.POST)
	@ResponseBody
	public String loadDistrictList(HttpServletRequest request, HttpServletResponse response, Model model) {

		Map<String, Object> jsonData = new HashMap<>();
		try {
			List<ListVo> districtList = ssmInventoryService.loadDistrictListForDropDown();
			System.out.println("Load District List:");
			districtList.forEach(s -> System.out.println(s.getListKey() + " " + s.getListValue()));

			jsonData.put("districtList", districtList);
		} catch (Exception e) {
			System.out.println(e);
			jsonData.put("districtList", "");
		}
		return new Gson().toJson(jsonData);
	}

	@RequestMapping(value = "/ssmInventoryCategoryList", method = RequestMethod.POST)
	@ResponseBody
	public String loadCategoryList(HttpServletRequest request, HttpServletResponse response, Model model) {

		Map<String, Object> jsonData = new HashMap<>();
		try {
			List<ListVo> categoryList = ssmInventoryService.loadCategoryListForDropDown();
			System.out.println("Load Category List:");
			categoryList.forEach(s -> System.out.println(s.getListKey() + " " + s.getListValue()));

			jsonData.put("categoryList", categoryList);
		} catch (Exception e) {
			System.out.println(e);
			jsonData.put("categoryList", "");
		}
		return new Gson().toJson(jsonData);
	}

	/**
	 **** This method is used to save ssm inventory info.
	 **** 
	 * @author Joy Roy <joyroylightoj@gmail.com>
	 **/

	@PostMapping(value = "/ssmInventorySave")
	public ModelAndView ssmInventorySave(ModelAndView model, SsmInventoryVo ssmInventoryVo,
			Authentication authentication, RedirectAttributes atts) {

		String message = "";
		String success = "";

		System.out.println("From ssmInventorySave Method: " + ssmInventoryVo);
		try {
			String userId = authentication.getName();
			Map<String, String> spFeedBack = ssmInventoryService.executeInventorySaveSp(ssmInventoryVo, userId);

			if (spFeedBack.get("P_OUT").equals("0")) {
				if (ssmInventoryVo.getActivityType().equals("I")) {
					success = "Stamp Sales Inventory Saved Successfully.";
				} else {
					success = "Stamp Sales Inventory Updated Successfully.";
				}
			} else {
				message = spFeedBack.get("P_ERROR_MESSAGE");
			}
		} catch (Exception e) {
			System.out.println(e);
		}

		System.out.println("success: " + success);
		atts.addFlashAttribute("message", message);
		atts.addFlashAttribute("success", success);
		return new ModelAndView("redirect:/ssminventory/ssmInventoryList");
	}

	/**
	 **** This method is used to paginated ssm inventory info.
	 **** 
	 * @author Joy Roy <joyroylightoj@gmail.com>
	 **/

	@RequestMapping(value = "/ssmInventoryList/paginated", method = RequestMethod.GET)
	@ResponseBody
	public String listUsersPaginatedForInventory(HttpServletRequest request, HttpServletResponse response, Model model,
			Authentication authentication) {

		DataTableRequest<User> dataTableInRQ = new DataTableRequest<User>(request);
		String userId = authentication.getName();
		PaginationCriteria pagination = dataTableInRQ.getPaginationRequest();
		String baseQuery = "select s.inv_id,\r\n" + "       s.district_id,\r\n" + "       (select d.district_name\r\n"
				+ "          from ssm_district d\r\n"
				+ "         where d.district_id = s.district_id)    district_name,\r\n" + "        s.category_id,\r\n"
				+ "        (select c.category_name from ssm_category c where c.category_id = s.category_id) category_name,\r\n"
				+ "        s.stock_cnt\r\n"
				+ "  from ssd_stock_inv s where s.district_id = (SELECT u.USER_SELLING_AREA\r\n"
				+ "                          FROM user_master u\r\n" + "                         WHERE u.USER_ID = '"
				+ userId + "'" + " )   order by s.inv_id";
		System.out.println(baseQuery);
		String paginatedQuery = AppUtil.buildPaginatedQueryForOracle(baseQuery, pagination);

		// System.out.println(paginatedQuery);

		Query query = entityManager.createNativeQuery(paginatedQuery, SsmInventoryModel.class);

		@SuppressWarnings("unchecked")
		List<SsmInventoryModel> userList = query.getResultList();

		DataTableResults<SsmInventoryModel> dataTableResult = new DataTableResults<SsmInventoryModel>();
		dataTableResult.setDraw(dataTableInRQ.getDraw());
		dataTableResult.setListOfDataObjects(userList);
		if (!AppUtil.isObjectEmpty(userList)) {
			dataTableResult.setRecordsTotal(userList.get(0).getTotalRecords().toString());
			if (dataTableInRQ.getPaginationRequest().isFilterByEmpty()) {
				dataTableResult.setRecordsFiltered(userList.get(0).getTotalRecords().toString());
			} else {
				dataTableResult.setRecordsFiltered(Integer.toString(userList.size()));
			}
		}
		return new Gson().toJson(dataTableResult);
	}

	@RequestMapping(value = "/ssmInventoryDasboardList", method = RequestMethod.GET)
	public ModelAndView landOnDisplayInventoryDashboardList(ModelAndView model, Authentication authentication) {

		authentication.getAuthorities().forEach(s -> roleName = s.getAuthority());

		try {
			List<ListVo> menuList = menuMasterService.loadMenuListByRole(roleName);
			model.addObject("menuList", menuList);

			menuList.forEach(m -> System.out.println(m.getListKey() + " " + m.getListValue()));

			List<ListVo> inventoryDashboardList = ssmInventoryService
					.loadInventoryDashBoardList(authentication.getName());
			
			if(inventoryDashboardList.size()==0)
			{
		        	ListVo listVo = new ListVo();
		        	listVo.setListKey("No Category Available");
		        	listVo.setListValue("No Remaining Stock Available");
		        	inventoryDashboardList.add(listVo);
			}

			model.addObject("inventoryDashboardList", inventoryDashboardList);
			
			UserMasterVo userMasterVo = appUserService.loadUserDistrictByUserId(authentication.getName());
			
			model.addObject("district", "Inventory DashBoard of " + userMasterVo.getUserSellingArea() + " District");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		

		model.setViewName("SS_Inventory/inventoryDashboard.html");

		return model;
	}
	
	
	@RequestMapping(value = "/ssmInventoryDasboardListForJoypurhat", method = RequestMethod.GET)
	public ModelAndView landOnDisplayInventoryDashboardListOpen(ModelAndView model) {

		try {

			List<ListVo> inventoryDashboardList = ssmInventoryService
					.loadInventoryDashBoardList("");
			
			if(inventoryDashboardList.size()==0)
			{
		        	ListVo listVo = new ListVo();
		        	listVo.setListKey("No Category Available");
		        	listVo.setListValue("No Remaining Stock Available");
		        	inventoryDashboardList.add(listVo);
			}
			
			//System.out.println("SIze: " + inventoryDashboardList.size());

			model.addObject("inventoryDashboardList", inventoryDashboardList);
			
			model.addObject("district", "Inventory DashBoard of Joypurhat District");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		model.setViewName("SS_Inventory/inventoryDashboardForJoypurhat.html");

		return model;
	}

}
