package com.joy.auth.controller;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.result.view.RedirectView;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.gson.Gson;
import com.joy.auth.common.ListVo;
import com.joy.auth.domain.EmployeesModel;
import com.joy.auth.domain.SsmCategoryModel;
import com.joy.auth.model.Customer;
import com.joy.auth.pagination.DataTableRequest;
import com.joy.auth.pagination.DataTableResults;
import com.joy.auth.pagination.PaginationCriteria;
import com.joy.auth.service.MenuMasterService;
import com.joy.auth.service.SsmCategoryService;
import com.joy.auth.utils.AppUtil;
import com.joy.auth.vo.SsmCategoryVo;
@RestController
@RequestMapping("/ssmcategory")
public class StampSalesCategoryController {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	SsmCategoryService ssmCategoryService;
	
	@Autowired
	MenuMasterService menuMasterService;
	
	private String roleName;
	
	@RequestMapping(value="/ssmCategoryList", method=RequestMethod.GET)
	public ModelAndView listUsersOracle(ModelAndView model, Authentication authentication) {
		
		authentication.getAuthorities().forEach(s -> roleName = s.getAuthority());

		try {
			List<ListVo> menuList = menuMasterService.loadMenuListByRole(roleName);
			model.addObject("menuList", menuList);

			menuList.forEach(m -> System.out.println(m.getListKey() + " " + m.getListValue()));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		model.setViewName("SS_Category/stampSalesCategory-list");
		return model;
	}
    
	@RequestMapping(value="/ssmCategoryList/paginated", method=RequestMethod.GET)
	@ResponseBody
	public String listUsersPaginatedForOracle(HttpServletRequest request, HttpServletResponse response, Model model) {
		
		DataTableRequest<User> dataTableInRQ = new DataTableRequest<User>(request);
		PaginationCriteria pagination = dataTableInRQ.getPaginationRequest();
		String baseQuery = "select category_id,category_name,category_description from ssm_category s order by category_id asc";
		String paginatedQuery = AppUtil.buildPaginatedQueryForOracle(baseQuery, pagination);
		
		System.out.println(paginatedQuery);
		
		Query query = entityManager.createNativeQuery(paginatedQuery, SsmCategoryModel.class);
		
		@SuppressWarnings("unchecked")
		List<SsmCategoryModel> userList = query.getResultList();
		
		DataTableResults<SsmCategoryModel> dataTableResult = new DataTableResults<SsmCategoryModel>();
		dataTableResult.setDraw(dataTableInRQ.getDraw());
		dataTableResult.setListOfDataObjects(userList);
		if (!AppUtil.isObjectEmpty(userList)) {
			dataTableResult.setRecordsTotal(userList.get(0).getTotalRecords()
					.toString());
			if (dataTableInRQ.getPaginationRequest().isFilterByEmpty()) {
				dataTableResult.setRecordsFiltered(userList.get(0).getTotalRecords()
						.toString());
			} else {
				dataTableResult.setRecordsFiltered(Integer.toString(userList.size()));
			}
		}
		return new Gson().toJson(dataTableResult);
	}
	
	
	@PostMapping(value = "/ssmCategorySave")
	public ModelAndView ssmCategorySave(ModelAndView model, SsmCategoryVo smCategoryVo, Authentication authentication, RedirectAttributes atts) {

		
		String message="";
		String success="";
		
		System.out.println("From ssmCategorySave Method: " + smCategoryVo);
		try {
			String userId = authentication.getName();
			Map<String,String> spFeedBack = ssmCategoryService.executeCategorySaveSp(smCategoryVo, userId);
			
			if(spFeedBack.get("P_OUT").equals("0"))
			{
				if(smCategoryVo.getActivityType().equals("I"))
				{
					success = "Stamp Sales Category Saved Successfully.";
				}
				else
				{
					success = "Stamp Sales Category Updated Successfully.";
				}
			}
			else
			{
				message = spFeedBack.get("P_ERROR_MESSAGE");
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		
		System.out.println("success: " + success);
		atts.addFlashAttribute("message", message);
		atts.addFlashAttribute("success", success);
		return new ModelAndView("redirect:/ssmcategory/ssmCategoryList");
	}

}
