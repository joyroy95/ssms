package com.joy.auth.controller;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.internal.build.AllowSysOut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.gson.Gson;
import com.joy.auth.common.ListVo;
import com.joy.auth.domain.SsmCategoryLimitModel;
import com.joy.auth.domain.SsmCategoryModel;
import com.joy.auth.domain.SsmInventoryModel;
import com.joy.auth.pagination.DataTableRequest;
import com.joy.auth.pagination.DataTableResults;
import com.joy.auth.pagination.PaginationCriteria;
import com.joy.auth.service.AppUserService;
import com.joy.auth.service.MenuMasterService;
import com.joy.auth.service.SsmCategoryLimitService;
import com.joy.auth.service.SsmInventoryService;
import com.joy.auth.utils.AppUtil;
import com.joy.auth.vo.SsmCategoryLimitVo;
import com.joy.auth.vo.SsmCategoryVo;
import com.joy.auth.vo.SsmInventoryVo;
import com.joy.auth.vo.UserMasterVo;

@RestController
@RequestMapping("/ssmcategorylmt")
public class StampSalesCategoryLimitController {

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	SsmCategoryLimitService ssmCategoryLimitService;

	@Autowired
	MenuMasterService menuMasterService;
	
	@Autowired
	AppUserService appUserService;

	private String roleName;

	@RequestMapping(value = "/ssmCategoryLimitList", method = RequestMethod.GET)
	public ModelAndView landOnDisplayCategoryLimitList(ModelAndView model, Authentication authentication) {

		authentication.getAuthorities().forEach(s -> roleName = s.getAuthority());

		try {
			List<ListVo> menuList = menuMasterService.loadMenuListByRole(roleName);
			model.addObject("menuList", menuList);

			menuList.forEach(m -> System.out.println(m.getListKey() + " " + m.getListValue()));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		model.setViewName("SS_Category_limit/stampSalesCategoryLimit-list.html");
		UserDetails userDetails = (UserDetails) authentication.getPrincipal();
		System.out.println("User has authorities: " + userDetails.getAuthorities());
		return model;
	}

	/**
	 **** This method is used to save ssm inventory info.
	 **** 
	 * @author Joy Roy <joyroylightoj@gmail.com>
	 **/

	@PostMapping(value = "/ssmCategoryLimitSave")
	public ModelAndView ssmCategoryLimitSave(ModelAndView model, SsmCategoryLimitVo ssmCategoryLimitVo,
			Authentication authentication, RedirectAttributes atts) {

		String message = "";
		String success = "";

		System.out.println("From ssmCategoryLimitSave Method: " + ssmCategoryLimitVo);
		try {
			String userId = authentication.getName();
			Map<String, String> spFeedBack = ssmCategoryLimitService.executeCategoryLimitSaveSp(ssmCategoryLimitVo, userId);

			if (spFeedBack.get("P_OUT").equals("0")) {
				if (ssmCategoryLimitVo.getActivityType().equals("I")) {
					success = "Stamp Sales Category Limit Saved Successfully.";
				} else {
					success = "Stamp Sales Category Limit Updated Successfully.";
				}
			} else {
				message = spFeedBack.get("P_ERROR_MESSAGE");
			}
		} catch (Exception e) {
			System.out.println(e);
		}

		System.out.println("success: " + success);
		atts.addFlashAttribute("message", message);
		atts.addFlashAttribute("success", success);
		return new ModelAndView("redirect:/ssmcategorylmt/ssmCategoryLimitList");
	}

	/**
	 **** This method is used to paginated ssm inventory info.
	 **** 
	 * @author Joy Roy <joyroylightoj@gmail.com>
	 **/

	@RequestMapping(value = "/ssmCategoryLimitList/paginated", method = RequestMethod.GET)
	@ResponseBody
	public String listUsersPaginatedForInventory(HttpServletRequest request, HttpServletResponse response, Model model,
			Authentication authentication) {

		DataTableRequest<User> dataTableInRQ = new DataTableRequest<User>(request);
		String userId = authentication.getName();
		PaginationCriteria pagination = dataTableInRQ.getPaginationRequest();
		String baseQuery = "select s.district_id,\r\n"
				+ "       (select d.district_name\r\n"
				+ "          from ssm_district d\r\n"
				+ "         where d.district_id = s.district_id)    district_name,\r\n"
				+ "        s.category_id,\r\n"
				+ "        (select c.category_name from ssm_category c where c.category_id = s.category_id) category_name,\r\n"
				+ "        s.LIMIT_CNT\r\n"
				+ "  from SSM_CATEGORY_LIMIT s where s.district_id = (SELECT u.USER_SELLING_AREA\r\n"
				+ "                          FROM user_master u\r\n"
				+ "                         WHERE u.USER_ID = '" + userId + "' )   order by category_name";
		
		System.out.println(baseQuery);
		String paginatedQuery = AppUtil.buildPaginatedQueryForOracle(baseQuery, pagination);

		// System.out.println(paginatedQuery);

		Query query = entityManager.createNativeQuery(paginatedQuery, SsmCategoryLimitModel.class);

		@SuppressWarnings("unchecked")
		List<SsmCategoryLimitModel> userList = query.getResultList();

		DataTableResults<SsmCategoryLimitModel> dataTableResult = new DataTableResults<SsmCategoryLimitModel>();
		dataTableResult.setDraw(dataTableInRQ.getDraw());
		dataTableResult.setListOfDataObjects(userList);
		if (!AppUtil.isObjectEmpty(userList)) {
			dataTableResult.setRecordsTotal(userList.get(0).getTotalRecords().toString());
			if (dataTableInRQ.getPaginationRequest().isFilterByEmpty()) {
				dataTableResult.setRecordsFiltered(userList.get(0).getTotalRecords().toString());
			} else {
				dataTableResult.setRecordsFiltered(Integer.toString(userList.size()));
			}
		}
		return new Gson().toJson(dataTableResult);
	}

}
