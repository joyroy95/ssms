package com.joy.auth.controller;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.Period;
import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.gson.Gson;
import com.joy.auth.common.ListVo;
import com.joy.auth.domain.StampOrderIndividualModel;
import com.joy.auth.domain.StampOrderPendingModel;
import com.joy.auth.pagination.DataTableRequest;
import com.joy.auth.pagination.DataTableResults;
import com.joy.auth.pagination.PaginationCriteria;
import com.joy.auth.service.AppUserService;
import com.joy.auth.service.MenuMasterService;
import com.joy.auth.service.StampSalesOrderService;
import com.joy.auth.utils.AppUtil;
import com.joy.auth.utils.EmailSend;
import com.joy.auth.vo.StampOrderVo;
import com.joy.auth.vo.UserMasterVo;

@RestController
@RequestMapping("/ssmorder")
public class StampSalesOrderController {
	
	@Autowired
	EmailSend emailSend;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	StampSalesOrderService stampSalesOrderService;
	
	@Autowired
	MenuMasterService menuMasterService;
	
	@Autowired
	AppUserService appUserService;
	
	private String roleName;

	
	@RequestMapping(value="/ssmIndividualOrderList", method=RequestMethod.GET)
	public ModelAndView landOnDisplayIndividualOrderList(ModelAndView model, Authentication authentication) {
		authentication.getAuthorities().forEach(s -> roleName = s.getAuthority());

		try {
			List<ListVo> menuList = menuMasterService.loadMenuListByRole(roleName);
			model.addObject("menuList", menuList);

			menuList.forEach(m -> System.out.println(m.getListKey() + " " + m.getListValue()));
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		model.setViewName("Stamp_Order/stampOrder-list.html");
		UserDetails userDetails = (UserDetails) authentication.getPrincipal();
		System.out.println("User has authorities: " + userDetails.getAuthorities());
		
		/*
		 * LocalDate fromDate = LocalDate.of(2021, 1, 15); LocalDate toDate =
		 * LocalDate.now();
		 * 
		 * Period period = Period.between(fromDate, toDate);
		 * 
		 * System.out.println("Difference Between two dates: " + period.get);
		 */
		return model;
	}
	
	
	@RequestMapping(value="/ssmInventoryList", method=RequestMethod.POST)
	@ResponseBody
	public String loadInventoryList(HttpServletRequest request, HttpServletResponse response, Model model, Authentication authentication) {
		
		Map<String, Object> jsonData = new HashMap<>();
		String userId = authentication.getName();
		try {
			List<ListVo>  inventoryList = stampSalesOrderService.loadInventoryListForDropDown(userId);
			System.out.println("Load Category List:");
			inventoryList.forEach(s->System.out.println(s.getListKey()+ " " + s.getListValue()));
			
			jsonData.put("inventoryList", inventoryList);
		} catch (Exception e) {
			System.out.println(e);
			jsonData.put("inventoryList", "");
		}
		return new Gson().toJson(jsonData);
	}
	
	 /**
     ****This method is used to save stamp order.
     ****@author Joy Roy <joyroylightoj@gmail.com>
     **/
	
	@PostMapping(value = "/stampOrderSave")
	public ModelAndView stampOrderSave(ModelAndView model, StampOrderVo stampOrderVo, Authentication authentication, RedirectAttributes atts) {

		
		String message="";
		String success="";
		
		System.out.println("From stampOrderSave Method: " + stampOrderVo);
		try {
			String userId = authentication.getName();
			Map<String,String> spFeedBack = stampSalesOrderService.executeStampOrderSaveSp(stampOrderVo, userId);
			
			if(spFeedBack.get("P_OUT").equals("0"))
			{
				if(stampOrderVo.getActivityType().equals("I"))
				{
					success = "Stamp Order Saved Successfully.";
				}
				else
				{
					success = "Stamp Order Updated Successfully.";
				}
			}
			else
			{
				message = spFeedBack.get("P_ERROR_MESSAGE");
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		
		System.out.println("success: " + success);
		atts.addFlashAttribute("message", message);
		atts.addFlashAttribute("success", success);
		return new ModelAndView("redirect:/ssmorder/ssmIndividualOrderList");
	}
	
	 /**
     ****This method is used to paginated  individual stamp order list.
     ****@author Joy Roy <joyroylightoj@gmail.com>
     **/
	
	@RequestMapping(value="/stampOrderIndividualList/paginated", method=RequestMethod.GET)
	@ResponseBody
	public String listUsersPaginatedForSTampOrder(HttpServletRequest request, HttpServletResponse response, Model model, Authentication authentication) {
		
		DataTableRequest<User> dataTableInRQ = new DataTableRequest<User>(request);
		String userId = authentication.getName();
		PaginationCriteria pagination = dataTableInRQ.getPaginationRequest();
		String baseQuery = "SELECT s.SAL_ID,\r\n"
				+ "       (SELECT d.DISTRICT_NAME\r\n"
				+ "          FROM SSM_DISTRICT d\r\n"
				+ "         WHERE d.DISTRICT_ID = s.DISTRICT_ID)    DISTRICT_NAME,\r\n"
				+ "       (SELECT C.CATEGORY_NAME\r\n"
				+ "             FROM SSM_CATEGORY C\r\n"
				+ "            WHERE C.CATEGORY_ID = S.CATEGORY_ID) CATEGORY_NAME,\r\n"
				+ "        s.INV_ID,\r\n"
				+ "        s.SALE_AMT, \r\n"
				+ "        s.SALE_FEE,\r\n"
				+ "        s.SALE_TOTAL_AMT,\r\n"
				+ "        CASE WHEN S.order_status='P' and s.auth_status='U'\r\n"
				+ "        THEN 'Unauthorized'\r\n"
				+ "        WHEN S.order_status='A' and s.auth_status='A' \r\n"
				+ "        THEN 'Authorized'\r\n"
				+ "        ELSE 'Declined'\r\n"
				+ "        END order_status\r\n"
				+ "  FROM SSMD_SALES s where s.MAKER_ID = '"+userId + "' ORDER BY SAL_ID";
		System.out.println(baseQuery);
		String paginatedQuery = AppUtil.buildPaginatedQueryForOracle(baseQuery, pagination);
		
		
		
		Query query = entityManager.createNativeQuery(paginatedQuery, StampOrderIndividualModel.class);
		
		@SuppressWarnings("unchecked")
		List<StampOrderIndividualModel> userList = query.getResultList();
		
		DataTableResults<StampOrderIndividualModel> dataTableResult = new DataTableResults<StampOrderIndividualModel>();
		dataTableResult.setDraw(dataTableInRQ.getDraw());
		dataTableResult.setListOfDataObjects(userList);
		if (!AppUtil.isObjectEmpty(userList)) {
			dataTableResult.setRecordsTotal(userList.get(0).getTotalRecords()
					.toString());
			if (dataTableInRQ.getPaginationRequest().isFilterByEmpty()) {
				dataTableResult.setRecordsFiltered(userList.get(0).getTotalRecords()
						.toString());
			} else {
				dataTableResult.setRecordsFiltered(Integer.toString(userList.size()));
			}
		}
		return new Gson().toJson(dataTableResult);
	}
	
	@RequestMapping(value="/ssmPendingOrderList", method=RequestMethod.GET)
	public ModelAndView landOnDisplayPendingOrderList(ModelAndView model, Authentication authentication) {
		authentication.getAuthorities().forEach(s -> roleName = s.getAuthority());

		try {
			List<ListVo> menuList = menuMasterService.loadMenuListByRole(roleName);
			model.addObject("menuList", menuList);

			menuList.forEach(m -> System.out.println(m.getListKey() + " " + m.getListValue()));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		model.setViewName("Stamp_Order/pendingOrder-list.html");
		UserDetails userDetails = (UserDetails) authentication.getPrincipal();
		System.out.println("User has authorities: " + userDetails.getAuthorities());
		return model;
	}
	
	
	 /**
     ****This method is used to paginated pending stamp order list.
     ****@author Joy Roy <joyroylightoj@gmail.com>
     **/
	
	@RequestMapping(value="/stampOrderPendingList/paginated", method=RequestMethod.GET)
	@ResponseBody
	public String listUsersPaginatedForPendingStampOrder(HttpServletRequest request, HttpServletResponse response, Model model, Authentication authentication) {
		
		DataTableRequest<User> dataTableInRQ = new DataTableRequest<User>(request);
		String userId = authentication.getName();
		PaginationCriteria pagination = dataTableInRQ.getPaginationRequest();
		String baseQuery = "SELECT s.SAL_ID,\r\n"
				+ "       s.DISTRICT_ID,\r\n"
				+ "       (SELECT d.DISTRICT_NAME\r\n"
				+ "          FROM SSM_DISTRICT d\r\n"
				+ "         WHERE d.DISTRICT_ID = s.DISTRICT_ID)    DISTRICT_NAME,\r\n"
				+ "       S.CATEGORY_ID,\r\n"
				+ "       (SELECT C.CATEGORY_NAME\r\n"
				+ "          FROM SSM_CATEGORY C\r\n"
				+ "         WHERE C.CATEGORY_ID = S.CATEGORY_ID)    CATEGORY_NAME,\r\n"
				+ "       s.INV_ID,\r\n"
				+ "       s.SALE_AMT,\r\n"
				+ "       s.SALE_FEE,\r\n"
				+ "       s.SALE_TOTAL_AMT,\r\n"
				+ "       s.ORDER_BY,\r\n"
				+ "       s.ORDER_DATE\r\n"
				+ "       \r\n"
				+ "  FROM SSMD_SALES s\r\n"
				+ " WHERE     s.DISTRICT_ID = (SELECT u.USER_SELLING_AREA\r\n"
				+ "                              FROM user_master u\r\n"
				+ "                             WHERE u.USER_ID = '"+userId+"' )"
				+ "       AND s.AUTH_STATUS = 'U'\r\n"
				+ "       AND S.order_status = 'P'";
		System.out.println(baseQuery);
		String paginatedQuery = AppUtil.buildPaginatedQueryForOracle(baseQuery, pagination);
		
		
		
		Query query = entityManager.createNativeQuery(paginatedQuery, StampOrderPendingModel.class);
		
		@SuppressWarnings("unchecked")
		List<StampOrderPendingModel> userList = query.getResultList();
		
		DataTableResults<StampOrderPendingModel> dataTableResult = new DataTableResults<StampOrderPendingModel>();
		dataTableResult.setDraw(dataTableInRQ.getDraw());
		dataTableResult.setListOfDataObjects(userList);
		if (!AppUtil.isObjectEmpty(userList)) {
			dataTableResult.setRecordsTotal(userList.get(0).getTotalRecords()
					.toString());
			if (dataTableInRQ.getPaginationRequest().isFilterByEmpty()) {
				dataTableResult.setRecordsFiltered(userList.get(0).getTotalRecords()
						.toString());
			} else {
				dataTableResult.setRecordsFiltered(Integer.toString(userList.size()));
			}
		}
		return new Gson().toJson(dataTableResult);
	}
	
	@RequestMapping(value="/ssmPendingOrderAuthorize", method=RequestMethod.POST)
	@ResponseBody
	public String authorizePendingOrder(HttpServletResponse response, Model model, @RequestBody StampOrderVo stampOrderVo, Authentication authentication) {
		
		Map<String, Object> jsonData = new HashMap<>();
		try {
			String sal_id = stampOrderVo.getSal_id();
			String userId = authentication.getName();
			String order_status = stampOrderVo.getOrder_status();
			stampOrderVo = stampSalesOrderService.loadStampOrderBySalId(sal_id);
			stampOrderVo.setOrder_status(order_status);
			System.out.println("stampOrderVo from authorizePendingOrder: " + stampOrderVo);
			
			Map<String,String> spFeedBack = stampSalesOrderService.authorizeStampOrderSp(stampOrderVo, userId);
			
			String message = "";
			
			if(spFeedBack.get("P_OUT").equals("0"))
			{
				if(stampOrderVo.getOrder_status().equals("A"))
				{
				message = "Stamp Order Authorize Successfully.";
				}
				else if(stampOrderVo.getOrder_status().equals("D"))
				{
					message = "Stamp Order Declined Successfully.";
				}
				
				jsonData.put("status", "ok");
			}
			else
			{
				message = spFeedBack.get("P_ERROR_MESSAGE");
			}
			
			jsonData.put("message", message);
			
		} catch (Exception e) {
			System.out.println(e.getCause());
			jsonData.put("status", "");
			jsonData.put("message", "");
		}
		return new Gson().toJson(jsonData);
	}
	
	 /**
     ****This method is used to save stamp order.
     ****@author Joy Roy <joyroylightoj@gmail.com>
     **/
	
	@PostMapping(value = "/authorizeStampOrderSave")
	public ModelAndView authorizeStampOrderSave(ModelAndView model, StampOrderVo stampOrderVo, Authentication authentication, RedirectAttributes atts) {

		
		String message="";
		String success="";
		
		System.out.println("From authorizeStampOrderSave Method: " + stampOrderVo);
		try {
			
			String userId = authentication.getName();
			Date stampReceieveDate = stampOrderVo.getStamp_receive_date();
			String chalanNo = stampOrderVo.getChalan_no();
			stampOrderVo = stampSalesOrderService.loadStampOrderBySalId(stampOrderVo.getSal_id());
			stampOrderVo.setOrder_status("A");
			stampOrderVo.setStamp_receive_date(stampReceieveDate);
			stampOrderVo.setChalan_no(chalanNo);
			
			System.out.println("stampOrderVo from authorizeStampOrderSave: " + stampOrderVo);
			
			Map<String,String> spFeedBack = stampSalesOrderService.authorizeStampOrderSp(stampOrderVo, userId);
			if(spFeedBack.get("P_OUT").equals("0"))
			{
				success = "Stamp Order Authorize Successfully ";
				UserMasterVo userMasterVo;
				userMasterVo = appUserService.loadUserEmailByUserId(userId);
				
				emailSend.sendEmailForNotificationOfCollectionStamp(userMasterVo.getUserEmail(), stampReceieveDate, chalanNo);
				success  += "and an email is sent to customer for notify stamp receive date";
			}
			else
			{
				message = spFeedBack.get("P_ERROR_MESSAGE");
			}
			
		} catch (Exception e) {
			message+=e;
			System.out.println(e);
		}
		
		System.out.println("success: " + success);
		atts.addFlashAttribute("message", message);
		atts.addFlashAttribute("success", success);
		return new ModelAndView("redirect:/ssmorder/ssmPendingOrderList");
	}
	
	
	@RequestMapping(value="/ssmFetchDistrictId", method=RequestMethod.POST)
	@ResponseBody
	public String fetchDistrictIdByUserId(HttpServletResponse response, Model model, @RequestBody StampOrderVo stampOrderVo, Authentication authentication) {
		
		Map<String, Object> jsonData = new HashMap<>();
		try {
			String userId = authentication.getName();
			stampOrderVo = stampSalesOrderService.getDistrictIdByUserId(userId);
			System.out.println("stampOrderVo from fetchDistrictIdByUserId Controller: " + stampOrderVo);
			
			if(!stampOrderVo.getDistrict_id().isEmpty())
			{
				jsonData.put("districtId", stampOrderVo.getDistrict_id());
				jsonData.put("status", "ok");
			}
			
		} catch (Exception e) {
			System.out.println(e.getCause());
			jsonData.put("status", "");
			jsonData.put("districtId", "");
		}
		return new Gson().toJson(jsonData);
	}
	
	
	@RequestMapping(value="/ssmFetchInvIdByCate", method=RequestMethod.POST)
	@ResponseBody
	public String fetchInventoryIdByCategory(HttpServletResponse response, Model model, @RequestBody StampOrderVo stampOrderVo, Authentication authentication) {
		
		Map<String, Object> jsonData = new HashMap<>();
		try {
			stampOrderVo = stampSalesOrderService.getInventoryByCategory(stampOrderVo.getCategory_id(), stampOrderVo.getDistrict_id());
			System.out.println("stampOrderVo from fetchInventoryIdByCategory Controller: " + stampOrderVo);
			
			if(!stampOrderVo.getInv_id().isEmpty())
			{
				jsonData.put("invId", stampOrderVo.getInv_id());
				jsonData.put("status", "ok");
			}
			
		} catch (Exception e) {
			System.out.println(e.getCause());
			jsonData.put("status", "");
			jsonData.put("invId", "");
		}
		return new Gson().toJson(jsonData);
	}


}
