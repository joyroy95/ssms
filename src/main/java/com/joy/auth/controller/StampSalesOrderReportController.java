package com.joy.auth.controller;

import java.sql.SQLException;
import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.gson.Gson;
import com.joy.auth.common.ListVo;
import com.joy.auth.domain.StampOrderIndividualModel;
import com.joy.auth.domain.StampOrderIndividualReportModel;
import com.joy.auth.domain.StampOrderOfficialReportModel;
import com.joy.auth.domain.StampOrderPendingModel;
import com.joy.auth.pagination.DataTableRequest;
import com.joy.auth.pagination.DataTableResults;
import com.joy.auth.pagination.PaginationCriteria;
import com.joy.auth.service.AppUserService;
import com.joy.auth.service.MenuMasterService;
import com.joy.auth.service.StampSalesOrderReportService;
import com.joy.auth.service.StampSalesOrderService;
import com.joy.auth.utils.AppUtil;
import com.joy.auth.utils.EmailSend;
import com.joy.auth.vo.StampOrderReportVo;
import com.joy.auth.vo.StampOrderVo;
import com.joy.auth.vo.UserMasterVo;

@RestController
@RequestMapping("/ssmOrderReport")
public class StampSalesOrderReportController {
	
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	private MenuMasterService menuMasterService;

	@Autowired
	private StampSalesOrderReportService salesOrderReportService;
	
	private String roleName;

	
	@RequestMapping(value="/ssmIndividualOrderReportList", method=RequestMethod.GET)
	public ModelAndView landOnDisplayIndividualOrderList(ModelAndView model, Authentication authentication) {
		authentication.getAuthorities().forEach(s -> roleName = s.getAuthority());

		try {
			List<ListVo> menuList = menuMasterService.loadMenuListByRole(roleName);
			model.addObject("menuList", menuList);

			menuList.forEach(m -> System.out.println(m.getListKey() + " " + m.getListValue()));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		model.setViewName("SS_Report/stampOrder-list-Report-Customer.html");
		UserDetails userDetails = (UserDetails) authentication.getPrincipal();
		System.out.println("User has authorities: " + userDetails.getAuthorities());
		return model;
	}
	
	
	 /**
     ****This method is used to paginated  individual stamp order list.
     ****@author Joy Roy <joyroylightoj@gmail.com>
     **/
	
	@RequestMapping(value="/stampOrderIndividualReportList/paginated", method=RequestMethod.POST)
	@ResponseBody
	public String listUsersPaginatedForSTampOrder(HttpServletRequest request, HttpServletResponse response, Model model, Authentication authentication) {
		
		String from_date = request.getParameter("from_date");
		String to_date = request.getParameter("to_date");
		DataTableRequest<User> dataTableInRQ = new DataTableRequest<User>(request);
		String userId = authentication.getName();
		PaginationCriteria pagination = dataTableInRQ.getPaginationRequest();
		String baseQuery = "SELECT ROWNUM AS SL_NO,\r\n"
				+ "       (SELECT d.DISTRICT_NAME\r\n"
				+ "          FROM SSM_DISTRICT d\r\n"
				+ "         WHERE d.DISTRICT_ID = S.DISTRICT_ID)    DISTRICT_NAME,\r\n"
				+ "        (SELECT C.CATEGORY_NAME\r\n"
				+ "          FROM SSM_CATEGORY C\r\n"
				+ "         WHERE C.CATEGORY_ID = S.CATEGORY_ID)    CATEGORY_NAME,\r\n"
				+ "         SALE_AMT,\r\n"
				+ "         SALE_FEE,\r\n"
				+ "         S.SALE_TOTAL_AMT,\r\n"
				+ "         CHALAN_NO,\r\n"
				+ "         TO_CHAR(STAMP_RECEIVE_DATE, 'dd/MM/yyyy') STAMP_RECEIVE_DATE,\r\n"
				+ "         ORDER_BY,\r\n"
				+ "         TO_CHAR(ORDER_DATE, 'dd/MM/yyyy') ORDER_DATE,\r\n"
				+ "         SALE_BY,\r\n"
				+ "         TO_CHAR(SALE_DATE, 'dd/MM/yyyy') SALE_DATE,\r\n"
				+ "         CASE WHEN ORDER_STATUS = 'D'\r\n"
				+ "         THEN 'Declined'\r\n"
				+ "         WHEN ORDER_STATUS = 'A'\r\n"
				+ "         THEN 'Approved'\r\n"
				+ "         ELSE 'UnAuthorized'\r\n"
				+ "         END ORDER_STATUS\r\n"
				+ "         \r\n"
				+ "  FROM SSMD_SALES S WHERE S.MAKER_ID = '" + userId +"' AND TRUNC (MAKER_TIME) >= TO_DATE ('"+from_date + "', 'dd/mm/yyyy')"
				+ "  AND TRUNC (MAKER_TIME) <= TO_DATE ('"+to_date+"', 'dd/mm/yyyy')";
		System.out.println(baseQuery);
		String paginatedQuery = AppUtil.buildPaginatedQueryForOracle(baseQuery, pagination);
		
		
		
		Query query = entityManager.createNativeQuery(paginatedQuery, StampOrderIndividualReportModel.class);
		
		@SuppressWarnings("unchecked")
		List<StampOrderIndividualReportModel> userList = query.getResultList();
		
		DataTableResults<StampOrderIndividualReportModel> dataTableResult = new DataTableResults<StampOrderIndividualReportModel>();
		dataTableResult.setDraw(dataTableInRQ.getDraw());
		dataTableResult.setListOfDataObjects(userList);
		if (!AppUtil.isObjectEmpty(userList)) {
			dataTableResult.setRecordsTotal(userList.get(0).getTotalRecords()
					.toString());
			if (dataTableInRQ.getPaginationRequest().isFilterByEmpty()) {
				dataTableResult.setRecordsFiltered(userList.get(0).getTotalRecords()
						.toString());
			} else {
				dataTableResult.setRecordsFiltered(Integer.toString(userList.size()));
			}
		}
		return new Gson().toJson(dataTableResult);
	}
	
	@RequestMapping(value="/ssmOfficialStampOrderReportList", method=RequestMethod.GET)
	public ModelAndView landOnDisplayPendingOrderList(ModelAndView model, Authentication authentication) {
		authentication.getAuthorities().forEach(s -> roleName = s.getAuthority());

		try {
			List<ListVo> menuList = menuMasterService.loadMenuListByRole(roleName);
			model.addObject("menuList", menuList);

			menuList.forEach(m -> System.out.println(m.getListKey() + " " + m.getListValue()));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		model.setViewName("SS_Report/stampOrder-list-Report-Official.html");
		UserDetails userDetails = (UserDetails) authentication.getPrincipal();
		System.out.println("User has authorities: " + userDetails.getAuthorities());
		return model;
	}
	
	
	 /**
     ****This method is used to paginated pending stamp order list.
     ****@author Joy Roy <joyroylightoj@gmail.com>
     **/
	
	@RequestMapping(value="/stampOrderOfficialStampOrderReportList/paginated", method = RequestMethod.POST)
	@ResponseBody
	public String listUsersPaginatedForPendingStampOrder(HttpServletRequest request, HttpServletResponse response, 
			Model model, Authentication authentication) {
		String from_date = request.getParameter("from_date");
		String to_date = request.getParameter("to_date");
		System.out.println("from_date: " + request.getParameter("from_date"));
		System.out.println("to_date: " + request.getParameter("to_date"));
		//System.out.println(stampOrderReportVo.getFrom_date() + " " + stampOrderReportVo.getTo_date());
		DataTableRequest<User> dataTableInRQ = new DataTableRequest<User>(request);
		String userId = authentication.getName();
		PaginationCriteria pagination = dataTableInRQ.getPaginationRequest();
		String baseQuery = "SELECT ROWNUM AS SL_NO,\r\n"
				+ "       (SELECT d.DISTRICT_NAME\r\n"
				+ "          FROM SSM_DISTRICT d\r\n"
				+ "         WHERE d.DISTRICT_ID = S.DISTRICT_ID)    DISTRICT_NAME,\r\n"
				+ "        (SELECT C.CATEGORY_NAME\r\n"
				+ "          FROM SSM_CATEGORY C\r\n"
				+ "         WHERE C.CATEGORY_ID = S.CATEGORY_ID)    CATEGORY_NAME,\r\n"
				+ "         SALE_AMT,\r\n"
				+ "         SALE_FEE,\r\n"
				+ "         S.SALE_TOTAL_AMT,\r\n"
				+ "         CHALAN_NO,\r\n"
				+ "         TO_CHAR(STAMP_RECEIVE_DATE, 'dd/MM/yyyy') STAMP_RECEIVE_DATE,\r\n"
				+ "         ORDER_BY,\r\n"
				+ "         TO_CHAR(ORDER_DATE, 'dd/MM/yyyy') ORDER_DATE,\r\n"
				+ "         SALE_BY,\r\n"
				+ "         TO_CHAR(SALE_DATE, 'dd/MM/yyyy') SALE_DATE,\r\n"
				+ "         CASE WHEN ORDER_STATUS = 'D'\r\n"
				+ "         THEN 'Declined'\r\n"
				+ "         WHEN ORDER_STATUS = 'A'\r\n"
				+ "         THEN 'Approved'\r\n"
				+ "         ELSE 'UnAuthorized'\r\n"
				+ "         END ORDER_STATUS\r\n"
				+ "         \r\n"
				+ "  FROM SSMD_SALES S WHERE s.DISTRICT_ID = (SELECT u.USER_SELLING_AREA\r\n"
				+ "				                             FROM user_master u\r\n"
				+ "				                           WHERE u.USER_ID = '" + userId + "'" + " ) AND TRUNC (MAKER_TIME) >= TO_DATE ('"+from_date + "', 'dd/mm/yyyy')\r\n"
			    + "       AND TRUNC (MAKER_TIME) <= TO_DATE ('"+to_date+"', 'dd/mm/yyyy')";
		System.out.println(baseQuery);
		String paginatedQuery = AppUtil.buildPaginatedQueryForOracle(baseQuery, pagination);
		
		Query query = entityManager.createNativeQuery(paginatedQuery, StampOrderOfficialReportModel.class);
		
		@SuppressWarnings("unchecked")
		List<StampOrderOfficialReportModel> userList = query.getResultList();
		
		DataTableResults<StampOrderOfficialReportModel> dataTableResult = new DataTableResults<StampOrderOfficialReportModel>();
		dataTableResult.setDraw(dataTableInRQ.getDraw());
		dataTableResult.setListOfDataObjects(userList);
		if (!AppUtil.isObjectEmpty(userList)) {
			dataTableResult.setRecordsTotal(userList.get(0).getTotalRecords()
					.toString());
			if (dataTableInRQ.getPaginationRequest().isFilterByEmpty()) {
				dataTableResult.setRecordsFiltered(userList.get(0).getTotalRecords()
						.toString());
			} else {
				dataTableResult.setRecordsFiltered(Integer.toString(userList.size()));
			}
		}
		return new Gson().toJson(dataTableResult);
	}
	

}
