package com.joy.auth.controller;

import java.sql.SQLException;
import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.gson.Gson;
import com.joy.auth.common.ListVo;
import com.joy.auth.domain.StampOrderIndividualModel;
import com.joy.auth.domain.StampOrderIndividualReportModel;
import com.joy.auth.domain.StampOrderOfficialReportModel;
import com.joy.auth.domain.StampOrderPendingModel;
import com.joy.auth.domain.StampSalesTotalSalesReportModel;
import com.joy.auth.pagination.DataTableRequest;
import com.joy.auth.pagination.DataTableResults;
import com.joy.auth.pagination.PaginationCriteria;
import com.joy.auth.service.AppUserService;
import com.joy.auth.service.MenuMasterService;
import com.joy.auth.service.StampSalesOrderReportService;
import com.joy.auth.service.StampSalesOrderService;
import com.joy.auth.utils.AppUtil;
import com.joy.auth.utils.EmailSend;
import com.joy.auth.vo.StampOrderReportVo;
import com.joy.auth.vo.StampOrderVo;
import com.joy.auth.vo.UserMasterVo;

@RestController
@RequestMapping("/ssmTotalSales")
public class StampSalesTotalSalesReportController {
	
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	private MenuMasterService menuMasterService;

	@Autowired
	private StampSalesOrderReportService salesOrderReportService;
	
	private String roleName;

	
	@RequestMapping(value="/ssmCategoryWiseTotalSales", method=RequestMethod.GET)
	public ModelAndView landOnDisplayIndividualOrderList(ModelAndView model, Authentication authentication) {
		authentication.getAuthorities().forEach(s -> roleName = s.getAuthority());

		try {
			List<ListVo> menuList = menuMasterService.loadMenuListByRole(roleName);
			model.addObject("menuList", menuList);

			menuList.forEach(m -> System.out.println(m.getListKey() + " " + m.getListValue()));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		model.setViewName("SS_Report/ss_total_sales_report.html");
		UserDetails userDetails = (UserDetails) authentication.getPrincipal();
		System.out.println("User has authorities: " + userDetails.getAuthorities());
		return model;
	}
	
	
	 /**
     ****This method is used to paginated  individual stamp order list.
     ****@author Joy Roy <joyroylightoj@gmail.com>
     **/
	
	@RequestMapping(value="/ssmCategoryWiseTotalSalesReportList/paginated", method=RequestMethod.GET)
	@ResponseBody
	public String listUsersPaginatedForTotalSales(HttpServletRequest request, HttpServletResponse response, Model model, Authentication authentication) {
		
		DataTableRequest<User> dataTableInRQ = new DataTableRequest<User>(request);
		String userId = authentication.getName();
		PaginationCriteria pagination = dataTableInRQ.getPaginationRequest();
		String baseQuery = "  select rownum sl_no, a.* from (SELECT \r\n"
				+ "    (SELECT C.CATEGORY_NAME\r\n"
				+ "            FROM SSM_CATEGORY C where C.CATEGORY_ID = s.CATEGORY_ID) CATEGORY_NAME,\r\n"
				+ "         SUM (s.SALE_TOTAL_AMT) TOTAL_SALE\r\n"
				+ "    FROM ssmd_sales s\r\n"
				+ "   WHERE district_id = (SELECT U.USER_SELLING_AREA FROM USER_MASTER U WHERE U.USER_ID = '" + userId + "')\r\n"
				+ "GROUP BY category_id) a";
		System.out.println(baseQuery);
		String paginatedQuery = AppUtil.buildPaginatedQueryForOracle(baseQuery, pagination);
		
		
		Query query = entityManager.createNativeQuery(paginatedQuery, StampSalesTotalSalesReportModel.class);
		
		@SuppressWarnings("unchecked")
		List<StampSalesTotalSalesReportModel> userList = query.getResultList();
		
		DataTableResults<StampSalesTotalSalesReportModel> dataTableResult = new DataTableResults<StampSalesTotalSalesReportModel>();
		dataTableResult.setDraw(dataTableInRQ.getDraw());
		dataTableResult.setListOfDataObjects(userList);
		if (!AppUtil.isObjectEmpty(userList)) {
			dataTableResult.setRecordsTotal(userList.get(0).getTotalRecords()
					.toString());
			if (dataTableInRQ.getPaginationRequest().isFilterByEmpty()) {
				dataTableResult.setRecordsFiltered(userList.get(0).getTotalRecords()
						.toString());
			} else {
				dataTableResult.setRecordsFiltered(Integer.toString(userList.size()));
			}
		}
		return new Gson().toJson(dataTableResult);
	}

}
