package com.joy.auth.controller;

import java.security.Principal;
import java.sql.SQLException;
import java.util.List;

import com.joy.auth.common.ListVo;
import com.joy.auth.service.AppUserService;
import com.joy.auth.service.MenuMasterService;
import com.joy.auth.service.UserDetailsServiceImpl;
import com.joy.auth.utils.WebUtils;
import com.joy.auth.vo.UserMasterVo;

import org.apache.logging.log4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

@RestController
public class MainController {
	
	private Logger log = LogManager.getLogger(MainController.class);

	@Autowired
	UserDetailsServiceImpl userDetailsService;
	@Autowired
	private AppUserService setpassword;

	@Autowired
	private MenuMasterService menuMasterService;
	
	@Autowired
	AppUserService appUserService;

	private String roleName;

	@RequestMapping(value = "/admin", method = RequestMethod.GET)

	public ModelAndView adminPage(ModelAndView model, Principal principal) {

		User loginedUser = (User) ((Authentication) principal).getPrincipal();
		System.out.println("Login password: " + loginedUser);
		String userInfo = WebUtils.toString(loginedUser);
		System.out.println("Hello from admin " + principal.getName());
		model.addObject("userInfo", userInfo);

		model.setViewName("adminPage");

		return model;
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView loginPage(ModelAndView model) {
		log.info("Entering into Login Page.");
		model.setViewName("loginPage");
		log.info("Exiting from LoginPage");
		return model;
	}

	@RequestMapping(value = "/logoutSuccessful", method = RequestMethod.GET)
	public ModelAndView logoutSuccessfulPage(ModelAndView model) {
		model.addObject("title", "Logout");
		model.setViewName("loginPage");
		return model;
	}

	@RequestMapping(value = "/admin/changePassword", method = RequestMethod.GET)

	public ModelAndView getAdminChangePassWordPage(ModelAndView model, Authentication authentication) {

		authentication.getAuthorities().forEach(s -> roleName = s.getAuthority());

		try {
			List<ListVo> menuList = menuMasterService.loadMenuListByRole(roleName);
			model.addObject("menuList", menuList);

			menuList.forEach(m -> System.out.println(m.getListKey() + " " + m.getListValue()));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		model.addObject("user", authentication.getName());
		model.setViewName("changePasswordPage");

		return model;
	}

	@PostMapping(value = "/admin/changePassword")
	public ModelAndView AdminChangePassWordPage(ModelAndView model, Authentication authentication, String password,
			String new_password, RedirectAttributes atts) {

		String message = "";

		if (setpassword.checkAdminCurrentPassword(authentication.getName(), password)) {
			if (password.equals(new_password)) {
				message += "Existing Password and New password can't be Same";
			} else {
				setpassword.updatePassword(authentication.getName(), new_password);
				message += "Password is changed Successfully";
			}
		} else {
			message += "Existing Password is not Matched!!";
		}
		
		atts.addFlashAttribute("message", message);
		return new ModelAndView("redirect:/admin/changePassword");

	}

	@RequestMapping(value = "/403", method = RequestMethod.GET)
	public ModelAndView accessDenied(ModelAndView model, Principal principal) {

		if (principal != null) {
			User loginedUser = (User) ((Authentication) principal).getPrincipal();

			String userInfo = WebUtils.toString(loginedUser);

			model.addObject("userInfo", userInfo);

			String message = "You do not have permission to access this page!";
			model.addObject("message", message);
			System.out.println(message);

		}
		model.setViewName("loginPage");

		return model;
	}

	@GetMapping(value = "/")
	public ModelAndView getHomePage(ModelAndView model) {
		model.setViewName("loginPage");
		;
		return model;
	}

	@GetMapping(value = "/index")
	public ModelAndView getIndexPage(ModelAndView model, Authentication authentication, Principal principal) {
		// System.out.println("Authority: " + authentication.getAuthorities());

		authentication.getAuthorities().forEach(s -> roleName = s.getAuthority());

		try {
			List<ListVo> menuList = menuMasterService.loadMenuListByRole(roleName);
			model.addObject("menuList", menuList);

			menuList.forEach(m -> System.out.println(m.getListKey() + " " + m.getListValue()));
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		User loginedUser = (User) ((Authentication) principal).getPrincipal();
		
		String userInfo = WebUtils.toString(loginedUser);
		
		
		model.addObject("userInfo", userInfo);

		model.setViewName("index");
		return model;
	}

}
