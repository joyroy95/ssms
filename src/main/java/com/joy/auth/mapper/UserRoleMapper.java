package com.joy.auth.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.joy.auth.model.UserRole;

public class UserRoleMapper implements RowMapper<UserRole> {

	public static final String BASE_SQL //
			= "Select u.Id, u.User_Id, u.Role_Id From User_Role u ";

	@Override
	public UserRole mapRow(ResultSet rs, int rowNum) throws SQLException {

		int id = rs.getInt("Id");
		int userId = rs.getInt("User_Id");
		int roleId = rs.getInt("Role_Id");
		return new UserRole(id, userId, roleId);
	}

}
