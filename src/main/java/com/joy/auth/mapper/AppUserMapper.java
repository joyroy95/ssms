package com.joy.auth.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
 

import org.springframework.jdbc.core.RowMapper;

import com.joy.auth.model.AppUser;
 
public class AppUserMapper implements RowMapper<AppUser> {
 
    public static final String BASE_SQL //
            = "select u.USER_ID, u.USER_NAME, u.USER_PASSWORD from user_master u";
 
    @Override
    public AppUser mapRow(ResultSet rs, int rowNum) throws SQLException {
 
        String userId = rs.getString("USER_ID");
        String userName = rs.getString("USER_NAME");
        String encrytedPassword = rs.getString("USER_PASSWORD");
 
        return new AppUser(userId, userName, encrytedPassword);
    }
 
}
