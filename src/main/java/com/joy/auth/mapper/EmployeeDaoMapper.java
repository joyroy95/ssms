package com.joy.auth.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;


import com.joy.auth.model.AppUser;
import com.joy.auth.model.Employees;

import org.springframework.jdbc.core.RowMapper;

public class EmployeeDaoMapper implements RowMapper<Employees> {
	
	public static final String BASE_SQL //
    = "Select u.Employee_Id, u.First_Name, u.Last_Name From Employees u ";

	@Override
	public Employees mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		int Employee_Id = rs.getInt("Employee_Id");
        String First_Name = rs.getString("First_Name");
        String Last_Name = rs.getString("Last_Name");
 
        return new Employees(Employee_Id, First_Name, Last_Name);
		
	}
	

}
