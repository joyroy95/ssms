package com.joy.auth.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.joy.auth.model.AppRole;
import com.joy.auth.model.AppUser;

public class AppRoleMapper implements RowMapper<AppRole>{
	
	@Override
    public AppRole mapRow(ResultSet rs, int rowNum) throws SQLException {
 
        int roleId = rs.getInt("Role_Id");
        String roleName = rs.getString("Role_Name");
        return new AppRole (roleId, roleName);
    }

}
