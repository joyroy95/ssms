package com.joy.auth.domain;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
public class UserMasterModel {
	@Id
	private String user_id;
	private String user_name;
	private String district_name;
	private String user_status;
	private String auth_status;  
	
	private Integer totalRecords;

	@Transient
	private Integer rn;

	/**
	 * @return the user_id
	 */
	public String getUser_id() {
		return user_id;
	}

	/**
	 * @param user_id the user_id to set
	 */
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	/**
	 * @return the user_name
	 */
	public String getUser_name() {
		return user_name;
	}

	/**
	 * @param user_name the user_name to set
	 */
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	/**
	 * @return the district_name
	 */
	public String getDistrict_name() {
		return district_name;
	}

	/**
	 * @param district_name the district_name to set
	 */
	public void setDistrict_name(String district_name) {
		this.district_name = district_name;
	}

	/**
	 * @return the user_status
	 */
	public String getUser_status() {
		return user_status;
	}

	/**
	 * @param user_status the user_status to set
	 */
	public void setUser_status(String user_status) {
		this.user_status = user_status;
	}

	/**
	 * @return the auth_status
	 */
	public String getAuth_status() {
		return auth_status;
	}

	/**
	 * @param auth_status the auth_status to set
	 */
	public void setAuth_status(String auth_status) {
		this.auth_status = auth_status;
	}

	/**
	 * @return the totalRecords
	 */
	public Integer getTotalRecords() {
		return totalRecords;
	}

	/**
	 * @param totalRecords the totalRecords to set
	 */
	public void setTotalRecords(Integer totalRecords) {
		this.totalRecords = totalRecords;
	}

	/**
	 * @return the rn
	 */
	public Integer getRn() {
		return rn;
	}

	/**
	 * @param rn the rn to set
	 */
	public void setRn(Integer rn) {
		this.rn = rn;
	}

	
}
