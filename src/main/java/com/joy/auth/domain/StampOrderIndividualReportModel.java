package com.joy.auth.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
public class StampOrderIndividualReportModel {
	@Id
	private String sl_no;
	private String district_name;
	private String category_name;  
	private String sale_amt;
	private String sale_fee;
	private String sale_total_amt;
	private String chalan_no;
	private String stamp_receive_date;
	private String order_by;
	private String order_date;
	private String sale_by;
	private String sale_date;
	private String order_status;
	
	private Integer totalRecords;

	@Transient
	private Integer rn;

	/**
	 * @return the sl_no
	 */
	public String getSl_no() {
		return sl_no;
	}

	/**
	 * @param sl_no the sl_no to set
	 */
	public void setSl_no(String sl_no) {
		this.sl_no = sl_no;
	}

	/**
	 * @return the district_name
	 */
	public String getDistrict_name() {
		return district_name;
	}

	/**
	 * @param district_name the district_name to set
	 */
	public void setDistrict_name(String district_name) {
		this.district_name = district_name;
	}

	/**
	 * @return the category_name
	 */
	public String getCategory_name() {
		return category_name;
	}

	/**
	 * @param category_name the category_name to set
	 */
	public void setCategory_name(String category_name) {
		this.category_name = category_name;
	}

	/**
	 * @return the sale_amt
	 */
	public String getSale_amt() {
		return sale_amt;
	}

	/**
	 * @param sale_amt the sale_amt to set
	 */
	public void setSale_amt(String sale_amt) {
		this.sale_amt = sale_amt;
	}

	/**
	 * @return the sale_fee
	 */
	public String getSale_fee() {
		return sale_fee;
	}

	/**
	 * @param sale_fee the sale_fee to set
	 */
	public void setSale_fee(String sale_fee) {
		this.sale_fee = sale_fee;
	}

	/**
	 * @return the sale_total_amt
	 */
	public String getSale_total_amt() {
		return sale_total_amt;
	}

	/**
	 * @param sale_total_amt the sale_total_amt to set
	 */
	public void setSale_total_amt(String sale_total_amt) {
		this.sale_total_amt = sale_total_amt;
	}

	/**
	 * @return the chalan_no
	 */
	public String getChalan_no() {
		return chalan_no;
	}

	/**
	 * @param chalan_no the chalan_no to set
	 */
	public void setChalan_no(String chalan_no) {
		this.chalan_no = chalan_no;
	}

	/**
	 * @return the stamp_receive_date
	 */
	public String getStamp_receive_date() {
		return stamp_receive_date;
	}

	/**
	 * @param stamp_receive_date the stamp_receive_date to set
	 */
	public void setStamp_receive_date(String stamp_receive_date) {
		this.stamp_receive_date = stamp_receive_date;
	}

	/**
	 * @return the order_by
	 */
	public String getOrder_by() {
		return order_by;
	}

	/**
	 * @param order_by the order_by to set
	 */
	public void setOrder_by(String order_by) {
		this.order_by = order_by;
	}

	/**
	 * @return the order_date
	 */
	public String getOrder_date() {
		return order_date;
	}

	/**
	 * @param order_date the order_date to set
	 */
	public void setOrder_date(String order_date) {
		this.order_date = order_date;
	}

	/**
	 * @return the sale_by
	 */
	public String getSale_by() {
		return sale_by;
	}

	/**
	 * @param sale_by the sale_by to set
	 */
	public void setSale_by(String sale_by) {
		this.sale_by = sale_by;
	}

	/**
	 * @return the sale_date
	 */
	public String getSale_date() {
		return sale_date;
	}

	/**
	 * @param sale_date the sale_date to set
	 */
	public void setSale_date(String sale_date) {
		this.sale_date = sale_date;
	}

	/**
	 * @return the order_status
	 */
	public String getOrder_status() {
		return order_status;
	}

	/**
	 * @param order_status the order_status to set
	 */
	public void setOrder_status(String order_status) {
		this.order_status = order_status;
	}

	/**
	 * @return the totalRecords
	 */
	public Integer getTotalRecords() {
		return totalRecords;
	}

	/**
	 * @param totalRecords the totalRecords to set
	 */
	public void setTotalRecords(Integer totalRecords) {
		this.totalRecords = totalRecords;
	}

	/**
	 * @return the rn
	 */
	public Integer getRn() {
		return rn;
	}

	/**
	 * @param rn the rn to set
	 */
	public void setRn(Integer rn) {
		this.rn = rn;
	}

	
}
