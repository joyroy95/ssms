package com.joy.auth.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
public class SsmInventoryModel {
	@Id
	private String inv_id;
	private String district_id;
	private String district_name;
	private String category_id;
	private String category_name;                    
	private String stock_cnt;
	
	private Integer totalRecords;

	@Transient
	private Integer rn;

	/**
	 * @return the inv_id
	 */
	public String getInv_id() {
		return inv_id;
	}

	/**
	 * @param inv_id the inv_id to set
	 */
	public void setInv_id(String inv_id) {
		this.inv_id = inv_id;
	}

	/**
	 * @return the district_id
	 */
	public String getDistrict_id() {
		return district_id;
	}

	/**
	 * @param district_id the district_id to set
	 */
	public void setDistrict_id(String district_id) {
		this.district_id = district_id;
	}

	/**
	 * @return the district_name
	 */
	public String getDistrict_name() {
		return district_name;
	}

	/**
	 * @param district_name the district_name to set
	 */
	public void setDistrict_name(String district_name) {
		this.district_name = district_name;
	}

	/**
	 * @return the category_id
	 */
	public String getCategory_id() {
		return category_id;
	}

	/**
	 * @param category_id the category_id to set
	 */
	public void setCategory_id(String category_id) {
		this.category_id = category_id;
	}

	/**
	 * @return the category_name
	 */
	public String getCategory_name() {
		return category_name;
	}

	/**
	 * @param category_name the category_name to set
	 */
	public void setCategory_name(String category_name) {
		this.category_name = category_name;
	}

	/**
	 * @return the stock_cnt
	 */
	public String getStock_cnt() {
		return stock_cnt;
	}

	/**
	 * @param stock_cnt the stock_cnt to set
	 */
	public void setStock_cnt(String stock_cnt) {
		this.stock_cnt = stock_cnt;
	}

	/**
	 * @return the totalRecords
	 */
	public Integer getTotalRecords() {
		return totalRecords;
	}

	/**
	 * @param totalRecords the totalRecords to set
	 */
	public void setTotalRecords(Integer totalRecords) {
		this.totalRecords = totalRecords;
	}

	/**
	 * @return the rn
	 */
	public Integer getRn() {
		return rn;
	}

	/**
	 * @param rn the rn to set
	 */
	public void setRn(Integer rn) {
		this.rn = rn;
	}

		
}
