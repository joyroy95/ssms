package com.joy.auth.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
public class StampSalesTotalSalesReportModel {
	@Id
	private String sl_no;
	private String category_name;
	private String total_sale;
	
	private Integer totalRecords;

	@Transient
	private Integer rn;

	public String getSl_no() {
		return sl_no;
	}

	public void setSl_no(String sl_no) {
		this.sl_no = sl_no;
	}

	public String getCategory_name() {
		return category_name;
	}

	public void setCategory_name(String category_name) {
		this.category_name = category_name;
	}

	public String getTotal_sale() {
		return total_sale;
	}

	public void setTotal_sale(String total_sale) {
		this.total_sale = total_sale;
	}

	public Integer getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(Integer totalRecords) {
		this.totalRecords = totalRecords;
	}

	public Integer getRn() {
		return rn;
	}

	public void setRn(Integer rn) {
		this.rn = rn;
	}

	
	
}
