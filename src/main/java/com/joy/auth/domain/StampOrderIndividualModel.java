package com.joy.auth.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
public class StampOrderIndividualModel {
	@Id
	private String sal_id;
	private String district_name;
	private String category_name;  
	private String inv_id;
	private String sale_amt;
	private String sale_fee;
	private String sale_total_amt;
	private String order_status;
	
	private Integer totalRecords;

	@Transient
	private Integer rn;

	/**
	 * @return the sal_id
	 */
	public String getSal_id() {
		return sal_id;
	}

	/**
	 * @param sal_id the sal_id to set
	 */
	public void setSal_id(String sal_id) {
		this.sal_id = sal_id;
	}

	/**
	 * @return the district_name
	 */
	public String getDistrict_name() {
		return district_name;
	}

	/**
	 * @param district_name the district_name to set
	 */
	public void setDistrict_name(String district_name) {
		this.district_name = district_name;
	}

	/**
	 * @return the category_name
	 */
	public String getCategory_name() {
		return category_name;
	}

	/**
	 * @param category_name the category_name to set
	 */
	public void setCategory_name(String category_name) {
		this.category_name = category_name;
	}

	/**
	 * @return the inv_id
	 */
	public String getInv_id() {
		return inv_id;
	}

	/**
	 * @param inv_id the inv_id to set
	 */
	public void setInv_id(String inv_id) {
		this.inv_id = inv_id;
	}

	/**
	 * @return the sale_amt
	 */
	public String getSale_amt() {
		return sale_amt;
	}

	/**
	 * @param sale_amt the sale_amt to set
	 */
	public void setSale_amt(String sale_amt) {
		this.sale_amt = sale_amt;
	}

	/**
	 * @return the sale_fee
	 */
	public String getSale_fee() {
		return sale_fee;
	}

	/**
	 * @param sale_fee the sale_fee to set
	 */
	public void setSale_fee(String sale_fee) {
		this.sale_fee = sale_fee;
	}

	/**
	 * @return the sale_total_amt
	 */
	public String getSale_total_amt() {
		return sale_total_amt;
	}

	/**
	 * @param sale_total_amt the sale_total_amt to set
	 */
	public void setSale_total_amt(String sale_total_amt) {
		this.sale_total_amt = sale_total_amt;
	}

	/**
	 * @return the order_status
	 */
	public String getOrder_status() {
		return order_status;
	}

	/**
	 * @param order_status the order_status to set
	 */
	public void setOrder_status(String order_status) {
		this.order_status = order_status;
	}

	/**
	 * @return the totalRecords
	 */
	public Integer getTotalRecords() {
		return totalRecords;
	}

	/**
	 * @param totalRecords the totalRecords to set
	 */
	public void setTotalRecords(Integer totalRecords) {
		this.totalRecords = totalRecords;
	}

	/**
	 * @return the rn
	 */
	public Integer getRn() {
		return rn;
	}

	/**
	 * @param rn the rn to set
	 */
	public void setRn(Integer rn) {
		this.rn = rn;
	}


	

		
}
