/**
 * 
 */
package com.joy.auth.repo;

import java.util.List;

import com.joy.auth.domain.UserModel;

/**
 * @author pavan.solapure
 *
 */
public interface GenericRepo {
	
	List<UserModel> getUserModel();
	
	
}
