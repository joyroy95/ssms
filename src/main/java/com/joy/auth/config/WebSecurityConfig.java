package com.joy.auth.config;

import com.joy.auth.service.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
 
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
 
    @Autowired
    UserDetailsServiceImpl userDetailsService;
 
    public BCryptPasswordEncoder passwordEncoder() {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        return bCryptPasswordEncoder;
    }
     
     
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception { 
 
        // Setting Service to find User in the database.
        // And Setting PassswordEncoder
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());     
 
    }
 
    @Override
    protected void configure(HttpSecurity http) throws Exception {
 
        http.csrf().disable();
 
        // The pages does not require login
        http.authorizeRequests().antMatchers("/", "/login", "/logout").permitAll();
 
        // /userInfo page requires login as ROLE_USER or ROLE_ADMIN.
        // If no login, it will redirect to /login page.
        http.authorizeRequests().antMatchers("/admin/createNewUser").access("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')");
        http.authorizeRequests().antMatchers("/admin/assignNewRoleToUser").access("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')");
        http.authorizeRequests().antMatchers("/admin/resetPassword").access("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')");
        http.authorizeRequests().antMatchers("/userInfo/changePassword").access("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_USER')");
        
        //ssmcategory
        http.authorizeRequests().antMatchers("/ssmcategory/ssmCategoryList").access("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')");
        
        //ssminventory
        http.authorizeRequests().antMatchers("/ssminventory/ssmInventoryList").access("hasAnyRole('ROLE_SUPER_ADMIN', 'ROLE_ADMIN')");
        
        http.authorizeRequests().antMatchers("/ssmorder/ssmIndividualOrderList").access("hasAnyRole('ROLE_USER', 'ROLE_SUPER_ADMIN')");
        
        http.authorizeRequests().antMatchers("/ssmorder/ssmPendingOrderList").access("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')");
        
        http.authorizeRequests().antMatchers("/admin/ssmUserList").access("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')");
        
        http.authorizeRequests().antMatchers("/ssminventory/ssmInventoryDasboardList").access("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_USER')");
        
        http.authorizeRequests().antMatchers("/index").access("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_USER')");
        
        http.authorizeRequests().antMatchers("/ssmOrderReport/ssmOfficialStampOrderReportList").access("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')");
        
        http.authorizeRequests().antMatchers("/ssmOrderReport/ssmIndividualOrderReportList").access("hasAnyRole('ROLE_USER', 'ROLE_SUPER_ADMIN')");
        
        http.authorizeRequests().antMatchers("/ssmTotalSales/ssmCategoryWiseTotalSales").access("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')");
        
        http.authorizeRequests().antMatchers("/ssmcategorylmt/ssmCategoryLimitList").access("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')");
        
        
        
        // When the user has logged in as XX.
        // But access a page that requires role YY,
        // AccessDeniedException will be thrown.
        http.authorizeRequests().and().exceptionHandling().accessDeniedPage("/403");
 
        // Config for Login Form
        http.headers().defaultsDisabled().cacheControl();
        
        http.authorizeRequests().and().formLogin()//
                // Submit URL of login page.
                .loginProcessingUrl("/j_spring_security_check") // Submit URL
                .defaultSuccessUrl("/index")//
                .loginPage("/login")
                .failureUrl("/login?error=true")//
                .usernameParameter("username")//
                .passwordParameter("password")
                // Config for Logout Page
                .and().logout().logoutUrl("/logout").logoutSuccessUrl("/login");
    }
}
