package com.joy.auth.vo;

public class SsmCategoryVo {
		
	private String activityType;
	private String category_id;                 
	private String category_name;                    
	private String category_description;
	

	public SsmCategoryVo() {
		super();
	}


	public String getActivityType() {
		return activityType;
	}


	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}


	public String getCategory_id() {
		return category_id;
	}


	public void setCategory_id(String category_id) {
		this.category_id = category_id;
	}


	public String getCategory_name() {
		return category_name;
	}


	public void setCategory_name(String category_name) {
		this.category_name = category_name;
	}


	public String getCategory_description() {
		return category_description;
	}


	public void setCategory_description(String category_description) {
		this.category_description = category_description;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((activityType == null) ? 0 : activityType.hashCode());
		result = prime * result + ((category_description == null) ? 0 : category_description.hashCode());
		result = prime * result + ((category_id == null) ? 0 : category_id.hashCode());
		result = prime * result + ((category_name == null) ? 0 : category_name.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SsmCategoryVo other = (SsmCategoryVo) obj;
		if (activityType == null) {
			if (other.activityType != null)
				return false;
		} else if (!activityType.equals(other.activityType))
			return false;
		if (category_description == null) {
			if (other.category_description != null)
				return false;
		} else if (!category_description.equals(other.category_description))
			return false;
		if (category_id == null) {
			if (other.category_id != null)
				return false;
		} else if (!category_id.equals(other.category_id))
			return false;
		if (category_name == null) {
			if (other.category_name != null)
				return false;
		} else if (!category_name.equals(other.category_name))
			return false;
		return true;
	}


	@Override
	public String toString() {
		return "SsmCategoryVo [activityType=" + activityType + ", category_id=" + category_id + ", category_name="
				+ category_name + ", category_description=" + category_description + "]";
	}
	
	
}
