package com.joy.auth.vo;

public class SsmCategoryLimitVo {
	
	private String activityType;               
	private String district_id;          
	private String category_id;         
	private String limit_cnt;
	
	
	public SsmCategoryLimitVo() {
		super();
		// TODO Auto-generated constructor stub
	}


	public String getActivityType() {
		return activityType;
	}


	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}


	public String getDistrict_id() {
		return district_id;
	}


	public void setDistrict_id(String district_id) {
		this.district_id = district_id;
	}


	public String getCategory_id() {
		return category_id;
	}


	public void setCategory_id(String category_id) {
		this.category_id = category_id;
	}


	public String getLimit_cnt() {
		return limit_cnt;
	}


	public void setLimit_cnt(String limit_cnt) {
		this.limit_cnt = limit_cnt;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((activityType == null) ? 0 : activityType.hashCode());
		result = prime * result + ((category_id == null) ? 0 : category_id.hashCode());
		result = prime * result + ((district_id == null) ? 0 : district_id.hashCode());
		result = prime * result + ((limit_cnt == null) ? 0 : limit_cnt.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SsmCategoryLimitVo other = (SsmCategoryLimitVo) obj;
		if (activityType == null) {
			if (other.activityType != null)
				return false;
		} else if (!activityType.equals(other.activityType))
			return false;
		if (category_id == null) {
			if (other.category_id != null)
				return false;
		} else if (!category_id.equals(other.category_id))
			return false;
		if (district_id == null) {
			if (other.district_id != null)
				return false;
		} else if (!district_id.equals(other.district_id))
			return false;
		if (limit_cnt == null) {
			if (other.limit_cnt != null)
				return false;
		} else if (!limit_cnt.equals(other.limit_cnt))
			return false;
		return true;
	}


	@Override
	public String toString() {
		return "SsmCategoryLimitVo [activityType=" + activityType + ", district_id=" + district_id + ", category_id="
				+ category_id + ", limit_cnt=" + limit_cnt + "]";
	}

	

}
