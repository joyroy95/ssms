package com.joy.auth.vo;

public class SsmInventoryVo {
	
	private String activityType;
	private String inv_id;               
	private String district_id;          
	private String category_id;         
	private String stock_cnt;
	
	
	public SsmInventoryVo() {
		super();
		// TODO Auto-generated constructor stub
	}


	public String getActivityType() {
		return activityType;
	}


	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}


	public String getInv_id() {
		return inv_id;
	}


	public void setInv_id(String inv_id) {
		this.inv_id = inv_id;
	}


	public String getDistrict_id() {
		return district_id;
	}


	public void setDistrict_id(String district_id) {
		this.district_id = district_id;
	}


	public String getCategory_id() {
		return category_id;
	}


	public void setCategory_id(String category_id) {
		this.category_id = category_id;
	}


	public String getStock_cnt() {
		return stock_cnt;
	}


	public void setStock_cnt(String stock_cnt) {
		this.stock_cnt = stock_cnt;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((activityType == null) ? 0 : activityType.hashCode());
		result = prime * result + ((category_id == null) ? 0 : category_id.hashCode());
		result = prime * result + ((district_id == null) ? 0 : district_id.hashCode());
		result = prime * result + ((inv_id == null) ? 0 : inv_id.hashCode());
		result = prime * result + ((stock_cnt == null) ? 0 : stock_cnt.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SsmInventoryVo other = (SsmInventoryVo) obj;
		if (activityType == null) {
			if (other.activityType != null)
				return false;
		} else if (!activityType.equals(other.activityType))
			return false;
		if (category_id == null) {
			if (other.category_id != null)
				return false;
		} else if (!category_id.equals(other.category_id))
			return false;
		if (district_id == null) {
			if (other.district_id != null)
				return false;
		} else if (!district_id.equals(other.district_id))
			return false;
		if (inv_id == null) {
			if (other.inv_id != null)
				return false;
		} else if (!inv_id.equals(other.inv_id))
			return false;
		if (stock_cnt == null) {
			if (other.stock_cnt != null)
				return false;
		} else if (!stock_cnt.equals(other.stock_cnt))
			return false;
		return true;
	}


	@Override
	public String toString() {
		return "SsmInventoryVo [activityType=" + activityType + ", inv_id=" + inv_id + ", district_id=" + district_id
				+ ", category_id=" + category_id + ", stock_cnt=" + stock_cnt + "]";
	}

	
	
	


}
	