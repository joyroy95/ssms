package com.joy.auth.vo;

import java.sql.Date;

public class StampOrderVo {
	
	private String activityType;
	private String sal_id;
	private String inv_id;               
	private String district_id;          
	private String category_id;         
	private String sale_amt;
	private String sale_fee;
	private String sale_total_amt;
	private String order_status;
	private Date stamp_receive_date;
	private String chalan_no;
	
	/**
	 * @return the activityType
	 */
	public String getActivityType() {
		return activityType;
	}
	/**
	 * @param activityType the activityType to set
	 */
	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}
	/**
	 * @return the sale_id
	 */
	public String getSal_id() {
		return sal_id;
	}
	/**
	 * @param sale_id the sale_id to set
	 */
	public void setSal_id(String sal_id) {
		this.sal_id = sal_id;
	}
	/**
	 * @return the inv_id
	 */
	public String getInv_id() {
		return inv_id;
	}
	/**
	 * @param inv_id the inv_id to set
	 */
	public void setInv_id(String inv_id) {
		this.inv_id = inv_id;
	}
	/**
	 * @return the district_id
	 */
	public String getDistrict_id() {
		return district_id;
	}
	/**
	 * @param district_id the district_id to set
	 */
	public void setDistrict_id(String district_id) {
		this.district_id = district_id;
	}
	/**
	 * @return the category_id
	 */
	public String getCategory_id() {
		return category_id;
	}
	/**
	 * @param category_id the category_id to set
	 */
	public void setCategory_id(String category_id) {
		this.category_id = category_id;
	}
	/**
	 * @return the sale_amt
	 */
	public String getSale_amt() {
		return sale_amt;
	}
	/**
	 * @param sale_amt the sale_amt to set
	 */
	public void setSale_amt(String sale_amt) {
		this.sale_amt = sale_amt;
	}
	/**
	 * @return the sale_fee
	 */
	public String getSale_fee() {
		return sale_fee;
	}
	/**
	 * @param sale_fee the sale_fee to set
	 */
	public void setSale_fee(String sale_fee) {
		this.sale_fee = sale_fee;
	}
	/**
	 * @return the sale_total_amt
	 */
	public String getSale_total_amt() {
		return sale_total_amt;
	}
	/**
	 * @param sale_total_amt the sale_total_amt to set
	 */
	public void setSale_total_amt(String sale_total_amt) {
		this.sale_total_amt = sale_total_amt;
	}
	
	
	/**
	 * @return the order_status
	 */
	public String getOrder_status() {
		return order_status;
	}
	/**
	 * @param order_status the order_status to set
	 */
	public void setOrder_status(String order_status) {
		this.order_status = order_status;
	}
	
	
	/**
	 * @return the stamp_receive_date
	 */
	public Date getStamp_receive_date() {
		return stamp_receive_date;
	}
	/**
	 * @param stamp_receive_date the stamp_receive_date to set
	 */
	public void setStamp_receive_date(Date stamp_receive_date) {
		this.stamp_receive_date = stamp_receive_date;
	}
	/**
	 * @return the chalan_no
	 */
	public String getChalan_no() {
		return chalan_no;
	}
	/**
	 * @param chalan_no the chalan_no to set
	 */
	public void setChalan_no(String chalan_no) {
		this.chalan_no = chalan_no;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((activityType == null) ? 0 : activityType.hashCode());
		result = prime * result + ((category_id == null) ? 0 : category_id.hashCode());
		result = prime * result + ((chalan_no == null) ? 0 : chalan_no.hashCode());
		result = prime * result + ((district_id == null) ? 0 : district_id.hashCode());
		result = prime * result + ((inv_id == null) ? 0 : inv_id.hashCode());
		result = prime * result + ((order_status == null) ? 0 : order_status.hashCode());
		result = prime * result + ((sal_id == null) ? 0 : sal_id.hashCode());
		result = prime * result + ((sale_amt == null) ? 0 : sale_amt.hashCode());
		result = prime * result + ((sale_fee == null) ? 0 : sale_fee.hashCode());
		result = prime * result + ((sale_total_amt == null) ? 0 : sale_total_amt.hashCode());
		result = prime * result + ((stamp_receive_date == null) ? 0 : stamp_receive_date.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StampOrderVo other = (StampOrderVo) obj;
		if (activityType == null) {
			if (other.activityType != null)
				return false;
		} else if (!activityType.equals(other.activityType))
			return false;
		if (category_id == null) {
			if (other.category_id != null)
				return false;
		} else if (!category_id.equals(other.category_id))
			return false;
		if (chalan_no == null) {
			if (other.chalan_no != null)
				return false;
		} else if (!chalan_no.equals(other.chalan_no))
			return false;
		if (district_id == null) {
			if (other.district_id != null)
				return false;
		} else if (!district_id.equals(other.district_id))
			return false;
		if (inv_id == null) {
			if (other.inv_id != null)
				return false;
		} else if (!inv_id.equals(other.inv_id))
			return false;
		if (order_status == null) {
			if (other.order_status != null)
				return false;
		} else if (!order_status.equals(other.order_status))
			return false;
		if (sal_id == null) {
			if (other.sal_id != null)
				return false;
		} else if (!sal_id.equals(other.sal_id))
			return false;
		if (sale_amt == null) {
			if (other.sale_amt != null)
				return false;
		} else if (!sale_amt.equals(other.sale_amt))
			return false;
		if (sale_fee == null) {
			if (other.sale_fee != null)
				return false;
		} else if (!sale_fee.equals(other.sale_fee))
			return false;
		if (sale_total_amt == null) {
			if (other.sale_total_amt != null)
				return false;
		} else if (!sale_total_amt.equals(other.sale_total_amt))
			return false;
		if (stamp_receive_date == null) {
			if (other.stamp_receive_date != null)
				return false;
		} else if (!stamp_receive_date.equals(other.stamp_receive_date))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "StampOrderVo [activityType=" + activityType + ", sal_id=" + sal_id + ", inv_id=" + inv_id
				+ ", district_id=" + district_id + ", category_id=" + category_id + ", sale_amt=" + sale_amt
				+ ", sale_fee=" + sale_fee + ", sale_total_amt=" + sale_total_amt + ", order_status=" + order_status
				+ ", stamp_receive_date=" + stamp_receive_date + ", chalan_no=" + chalan_no + "]";
	}
	
	
}
