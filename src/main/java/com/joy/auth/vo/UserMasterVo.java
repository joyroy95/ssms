package com.joy.auth.vo;

import java.sql.Date;

public class UserMasterVo {
	
	private String activityType;
	private String userId;
	private String userName;
	private String userType;
	private String userPassword;
	private String userMobile;
	private String userEmail;
	private String userFathersName;
	private String userMothersName;
	private String userNid;
	private String userPermanentAddress;
	private String userPresentAddress;
	private String userSellingArea; // district
	private String userLicenseNo;
	private Date userLicenseIssueDate;
	private String userBankSolvency;
	private String userStatus;
	private String districtId;
	
	
	public UserMasterVo() {
		super();
		// TODO Auto-generated constructor stub
	}


	/**
	 * @return the activityType
	 */
	public String getActivityType() {
		return activityType;
	}


	/**
	 * @param activityType the activityType to set
	 */
	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}


	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}


	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}


	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}


	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}


	/**
	 * @return the userType
	 */
	public String getUserType() {
		return userType;
	}


	/**
	 * @param userType the userType to set
	 */
	public void setUserType(String userType) {
		this.userType = userType;
	}


	/**
	 * @return the userPassword
	 */
	public String getUserPassword() {
		return userPassword;
	}


	/**
	 * @param userPassword the userPassword to set
	 */
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}


	/**
	 * @return the userMobile
	 */
	public String getUserMobile() {
		return userMobile;
	}


	/**
	 * @param userMobile the userMobile to set
	 */
	public void setUserMobile(String userMobile) {
		this.userMobile = userMobile;
	}


	/**
	 * @return the userEmail
	 */
	public String getUserEmail() {
		return userEmail;
	}


	/**
	 * @param userEmail the userEmail to set
	 */
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}


	/**
	 * @return the userFathersName
	 */
	public String getUserFathersName() {
		return userFathersName;
	}


	/**
	 * @param userFathersName the userFathersName to set
	 */
	public void setUserFathersName(String userFathersName) {
		this.userFathersName = userFathersName;
	}


	/**
	 * @return the userMothersName
	 */
	public String getUserMothersName() {
		return userMothersName;
	}


	/**
	 * @param userMothersName the userMothersName to set
	 */
	public void setUserMothersName(String userMothersName) {
		this.userMothersName = userMothersName;
	}


	/**
	 * @return the userNid
	 */
	public String getUserNid() {
		return userNid;
	}


	/**
	 * @param userNid the userNid to set
	 */
	public void setUserNid(String userNid) {
		this.userNid = userNid;
	}


	/**
	 * @return the userPermanentAddress
	 */
	public String getUserPermanentAddress() {
		return userPermanentAddress;
	}


	/**
	 * @param userPermanentAddress the userPermanentAddress to set
	 */
	public void setUserPermanentAddress(String userPermanentAddress) {
		this.userPermanentAddress = userPermanentAddress;
	}


	/**
	 * @return the userPresentAddress
	 */
	public String getUserPresentAddress() {
		return userPresentAddress;
	}


	/**
	 * @param userPresentAddress the userPresentAddress to set
	 */
	public void setUserPresentAddress(String userPresentAddress) {
		this.userPresentAddress = userPresentAddress;
	}


	/**
	 * @return the userSellingArea
	 */
	public String getUserSellingArea() {
		return userSellingArea;
	}


	/**
	 * @param userSellingArea the userSellingArea to set
	 */
	public void setUserSellingArea(String userSellingArea) {
		this.userSellingArea = userSellingArea;
	}


	/**
	 * @return the userLicenseNo
	 */
	public String getUserLicenseNo() {
		return userLicenseNo;
	}


	/**
	 * @param userLicenseNo the userLicenseNo to set
	 */
	public void setUserLicenseNo(String userLicenseNo) {
		this.userLicenseNo = userLicenseNo;
	}


	/**
	 * @return the userLicenseIssueDate
	 */
	public Date getUserLicenseIssueDate() {
		return userLicenseIssueDate;
	}


	/**
	 * @param userLicenseIssueDate the userLicenseIssueDate to set
	 */
	public void setUserLicenseIssueDate(Date userLicenseIssueDate) {
		this.userLicenseIssueDate = userLicenseIssueDate;
	}


	/**
	 * @return the userBankSolvency
	 */
	public String getUserBankSolvency() {
		return userBankSolvency;
	}


	/**
	 * @param userBankSolvency the userBankSolvency to set
	 */
	public void setUserBankSolvency(String userBankSolvency) {
		this.userBankSolvency = userBankSolvency;
	}


	/**
	 * @return the userStatus
	 */
	public String getUserStatus() {
		return userStatus;
	}


	/**
	 * @param userStatus the userStatus to set
	 */
	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}
	
	
	
	


	/**
	 * @return the districtId
	 */
	public String getDistrictId() {
		return districtId;
	}


	/**
	 * @param districtId the districtId to set
	 */
	public void setDistrictId(String districtId) {
		this.districtId = districtId;
	}


	


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((activityType == null) ? 0 : activityType.hashCode());
		result = prime * result + ((districtId == null) ? 0 : districtId.hashCode());
		result = prime * result + ((userBankSolvency == null) ? 0 : userBankSolvency.hashCode());
		result = prime * result + ((userEmail == null) ? 0 : userEmail.hashCode());
		result = prime * result + ((userFathersName == null) ? 0 : userFathersName.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		result = prime * result + ((userLicenseIssueDate == null) ? 0 : userLicenseIssueDate.hashCode());
		result = prime * result + ((userLicenseNo == null) ? 0 : userLicenseNo.hashCode());
		result = prime * result + ((userMobile == null) ? 0 : userMobile.hashCode());
		result = prime * result + ((userMothersName == null) ? 0 : userMothersName.hashCode());
		result = prime * result + ((userName == null) ? 0 : userName.hashCode());
		result = prime * result + ((userNid == null) ? 0 : userNid.hashCode());
		result = prime * result + ((userPassword == null) ? 0 : userPassword.hashCode());
		result = prime * result + ((userPermanentAddress == null) ? 0 : userPermanentAddress.hashCode());
		result = prime * result + ((userPresentAddress == null) ? 0 : userPresentAddress.hashCode());
		result = prime * result + ((userSellingArea == null) ? 0 : userSellingArea.hashCode());
		result = prime * result + ((userStatus == null) ? 0 : userStatus.hashCode());
		result = prime * result + ((userType == null) ? 0 : userType.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserMasterVo other = (UserMasterVo) obj;
		if (activityType == null) {
			if (other.activityType != null)
				return false;
		} else if (!activityType.equals(other.activityType))
			return false;
		if (districtId == null) {
			if (other.districtId != null)
				return false;
		} else if (!districtId.equals(other.districtId))
			return false;
		if (userBankSolvency == null) {
			if (other.userBankSolvency != null)
				return false;
		} else if (!userBankSolvency.equals(other.userBankSolvency))
			return false;
		if (userEmail == null) {
			if (other.userEmail != null)
				return false;
		} else if (!userEmail.equals(other.userEmail))
			return false;
		if (userFathersName == null) {
			if (other.userFathersName != null)
				return false;
		} else if (!userFathersName.equals(other.userFathersName))
			return false;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		if (userLicenseIssueDate == null) {
			if (other.userLicenseIssueDate != null)
				return false;
		} else if (!userLicenseIssueDate.equals(other.userLicenseIssueDate))
			return false;
		if (userLicenseNo == null) {
			if (other.userLicenseNo != null)
				return false;
		} else if (!userLicenseNo.equals(other.userLicenseNo))
			return false;
		if (userMobile == null) {
			if (other.userMobile != null)
				return false;
		} else if (!userMobile.equals(other.userMobile))
			return false;
		if (userMothersName == null) {
			if (other.userMothersName != null)
				return false;
		} else if (!userMothersName.equals(other.userMothersName))
			return false;
		if (userName == null) {
			if (other.userName != null)
				return false;
		} else if (!userName.equals(other.userName))
			return false;
		if (userNid == null) {
			if (other.userNid != null)
				return false;
		} else if (!userNid.equals(other.userNid))
			return false;
		if (userPassword == null) {
			if (other.userPassword != null)
				return false;
		} else if (!userPassword.equals(other.userPassword))
			return false;
		if (userPermanentAddress == null) {
			if (other.userPermanentAddress != null)
				return false;
		} else if (!userPermanentAddress.equals(other.userPermanentAddress))
			return false;
		if (userPresentAddress == null) {
			if (other.userPresentAddress != null)
				return false;
		} else if (!userPresentAddress.equals(other.userPresentAddress))
			return false;
		if (userSellingArea == null) {
			if (other.userSellingArea != null)
				return false;
		} else if (!userSellingArea.equals(other.userSellingArea))
			return false;
		if (userStatus == null) {
			if (other.userStatus != null)
				return false;
		} else if (!userStatus.equals(other.userStatus))
			return false;
		if (userType == null) {
			if (other.userType != null)
				return false;
		} else if (!userType.equals(other.userType))
			return false;
		return true;
	}


	@Override
	public String toString() {
		return "UserMasterVo [activityType=" + activityType + ", userId=" + userId + ", userName=" + userName
				+ ", userType=" + userType + ", userPassword=" + userPassword + ", userMobile=" + userMobile
				+ ", userEmail=" + userEmail + ", userFathersName=" + userFathersName + ", userMothersName="
				+ userMothersName + ", userNid=" + userNid + ", userPermanentAddress=" + userPermanentAddress
				+ ", userPresentAddress=" + userPresentAddress + ", userSellingArea=" + userSellingArea
				+ ", userLicenseNo=" + userLicenseNo + ", userLicenseIssueDate=" + userLicenseIssueDate
				+ ", userBankSolvency=" + userBankSolvency + ", userStatus=" + userStatus + ", districtId=" + districtId
				+ "]";
	}


	
	
	
	

}
