package com.joy.auth.vo;

import java.sql.Date;

public class StampOrderReportVo {
	private String from_date;
	private String to_date;
	public String getFrom_date() {
		return from_date;
	}
	public void setFrom_date(String from_date) {
		this.from_date = from_date;
	}
	public String getTo_date() {
		return to_date;
	}
	public void setTo_date(String to_date) {
		this.to_date = to_date;
	}
	@Override
	public String toString() {
		return "StampOrderReportVo [from_date=" + from_date + ", to_date=" + to_date + "]";
	}
	
	
	

}
