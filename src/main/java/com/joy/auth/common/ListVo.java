package com.joy.auth.common;

public class ListVo {

	private String listKey;
	private String listValue;
	public ListVo() {
		super();
		// TODO Auto-generated constructor stub
	}
	/**
	 * @return the listKey
	 */
	public String getListKey() {
		return listKey;
	}
	/**
	 * @param listKey the listKey to set
	 */
	public void setListKey(String listKey) {
		this.listKey = listKey;
	}
	/**
	 * @return the listValue
	 */
	public String getListValue() {
		return listValue;
	}
	/**
	 * @param listValue the listValue to set
	 */
	public void setListValue(String listValue) {
		this.listValue = listValue;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((listKey == null) ? 0 : listKey.hashCode());
		result = prime * result + ((listValue == null) ? 0 : listValue.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ListVo other = (ListVo) obj;
		if (listKey == null) {
			if (other.listKey != null)
				return false;
		} else if (!listKey.equals(other.listKey))
			return false;
		if (listValue == null) {
			if (other.listValue != null)
				return false;
		} else if (!listValue.equals(other.listValue))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "ListVo [listKey=" + listKey + ", listValue=" + listValue + "]";
	}
	
	
}
