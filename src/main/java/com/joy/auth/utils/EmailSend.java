package com.joy.auth.utils;

import java.io.IOException;
import java.net.UnknownHostException;
import java.sql.Date;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class EmailSend {
	
	@Autowired
    private JavaMailSender javaMailSender;

	public  void sendEmail(String password, String email, String userName) throws MessagingException, IOException, UnknownHostException {

        SimpleMailMessage msg = new SimpleMailMessage();
        System.out.println("From EMail send class: " + email);
        msg.setTo(email);

        msg.setSubject("SSM Application User Password");
        msg.setText("SSM User ID: " + userName + "\n" + "Default password is: "+ password);

        javaMailSender.send(msg);

    }
	
	public  void sendEmailForNotificationOfCollectionStamp(String email, Date receiveDate, String chalanNo) throws MessagingException, IOException, UnknownHostException {

        SimpleMailMessage msg = new SimpleMailMessage();
        System.out.println("From EMail sendEmailForNotificationOfCollectionStamp: " + email);
        msg.setTo(email);

        msg.setSubject("Stamp Collection Notification");
        msg.setText("Dear Customer, \n Please Collect Stamp from district office on " + receiveDate + " and Your Chalan No is: "+ chalanNo);

        javaMailSender.send(msg);

    }

}
