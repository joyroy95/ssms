package com.joy.auth.utils;

import java.sql.SQLException;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.joy.auth.service.AppUserService;
import com.joy.auth.service.UserDetailsServiceImpl;
import com.joy.auth.vo.UserMasterVo;
 
public class WebUtils{
	@Autowired
	 UserDetailsServiceImpl userDetailsService;
	@Autowired
	static AppUserService appUserService;
	
    public static String toString(User user) {
        StringBuilder sb = new StringBuilder();
 
        sb.append("User ID : ").append(user.getUsername());
        sb.append(System.getProperty("line.separator"));
 
        Collection<GrantedAuthority> authorities = user.getAuthorities();
        if (authorities != null && !authorities.isEmpty()) {
        	sb.append(", Role : ");
            sb.append(" (");
            boolean first = true;
            for (GrantedAuthority a : authorities) {
                if (first) {
                    sb.append(a.getAuthority());
                    first = false;
                } else {
                    sb.append(", ").append(a.getAuthority());
                }
            }
            sb.append(")");
        }
        
        
        return sb.toString();
    }
    
   

}
