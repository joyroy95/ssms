package com.joy.auth.model;

public class Employees {
	
	private int Employee_Id;
	private String First_Name;
	private String Last_Name;
	
	public Employees(int employee_Id, String first_Name, String last_Name) {
		Employee_Id = employee_Id;
		First_Name = first_Name;
		Last_Name = last_Name;
	}
	public int getEmployee_Id() {
		return Employee_Id;
	}
	public void setEmployee_Id(int employee_Id) {
		Employee_Id = employee_Id;
	}
	public String getFirst_Name() {
		return First_Name;
	}
	public void setFirst_Name(String first_Name) {
		First_Name = first_Name;
	}
	public String getLast_Name() {
		return Last_Name;
	}
	public void setLast_Name(String last_Name) {
		Last_Name = last_Name;
	}
	
	

}
