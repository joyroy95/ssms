package com.joy.auth.model;

import java.sql.Date;

public class Customer {
	
	private int customerId;
	private String customerName;
	private String fathersName;
	private String mothersName;
	private Date dateOfBirth;
	private String address;
	private String phoneNumber;
	private String nidNumber;
	private String radio;
	private int branchCode;
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getFathersName() {
		return fathersName;
	}
	public void setFathersName(String fathersName) {
		this.fathersName = fathersName;
	}
	public String getMothersName() {
		return mothersName;
	}
	public void setMothersName(String mothersName) {
		this.mothersName = mothersName;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getNidNumber() {
		return nidNumber;
	}
	public void setNidNumber(String nidNumber) {
		this.nidNumber = nidNumber;
	}
	public String getAccountType() {
		return radio;
	}
	public void setAccountType(String accountType) {
		this.radio = accountType;
	}
	@Override
	public String toString() {
		return "Customer [customerName=" + customerName + ", fathersName=" + fathersName + ", mothersName="
				+ mothersName + ", dateOfBirth=" + dateOfBirth + ", address=" + address + ", phoneNumber=" + phoneNumber
				+ ", nidNumber=" + nidNumber + ", accountType=" + radio + "]";
	}
	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	public int getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(int branchCode) {
		this.branchCode = branchCode;
	}
}
