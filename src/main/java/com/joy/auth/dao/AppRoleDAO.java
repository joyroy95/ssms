package com.joy.auth.dao;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.joy.auth.mapper.AppRoleMapper;
import com.joy.auth.mapper.AppUserMapper;
import com.joy.auth.model.AppRole;
import com.joy.auth.model.AppUser;
 
@Repository
@Transactional
public class AppRoleDAO extends JdbcDaoSupport {
 
    @Autowired
    public AppRoleDAO(DataSource dataSource) {
        this.setDataSource(dataSource);
    }
    
    public static final String BASE_SQL //
    = "Select u.Role_Id, u.Role_Name From App_Role u ";
 
    public List<String> getRoleNames(String i) {
        String sql = "Select r.Role_Name " //
                + " from User_Role ur, App_Role r " //
                + " where ur.Role_Id = r.Role_Id and ur.User_Id = ? ";
 
        Object[] params = new Object[] { i };
 
        List<String> roles = this.getJdbcTemplate().queryForList(sql, params, String.class);
 
        return roles;
    }
    
    public List<String> getAllRoleNames() {
        String sql = "Select Role_Name " //
                + " from App_Role" ;
 
        Object[] params = new Object[] {};
 
        List<String> roles = this.getJdbcTemplate().queryForList(sql, params, String.class);
 
        return roles;
    }
        
    
    public AppRole findRoleAccount(String roleName) {
        // Select .. from App_User u Where u.User_Name = ?
        String sql = BASE_SQL + " where u.Role_Name = ? ";
 
        Object[] params = new Object[] { roleName };
        AppRoleMapper mapper = new AppRoleMapper();
        try {
            AppRole roleInfo = this.getJdbcTemplate().queryForObject(sql, params, mapper);
            return roleInfo;
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }
}
