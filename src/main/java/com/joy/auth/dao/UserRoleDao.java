package com.joy.auth.dao;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.joy.auth.mapper.AppUserMapper;
import com.joy.auth.mapper.UserRoleMapper;
import com.joy.auth.model.AppUser;
import com.joy.auth.model.UserRole;
@Repository
@Transactional
public class UserRoleDao extends JdbcDaoSupport {
	
	@Autowired
	public UserRoleDao(DataSource dataSource) {
	        this.setDataSource(dataSource);
	    }

}
