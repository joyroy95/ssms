package com.joy.auth.dao;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.joy.auth.mapper.EmployeeDaoMapper;
import com.joy.auth.model.Employees;

@Repository
@Transactional
public class EmployeeDao extends JdbcDaoSupport{
		
	@Autowired
    public EmployeeDao(DataSource dataSource) {
        this.setDataSource(dataSource);
    }
	
	 public Employees findEmployeeAccount(int Employee_Id) {
	        // Select .. from App_User u Where u.User_Name = ?
	        String sql = EmployeeDaoMapper.BASE_SQL + " where u.Employee_Id = ? ";
	 
	        Object[] params = new Object[] { Employee_Id };
	        EmployeeDaoMapper mapper = new EmployeeDaoMapper();
	        try {
	            Employees employeeInfo = this.getJdbcTemplate().queryForObject(sql, params, mapper);
	            return employeeInfo;
	        } catch (EmptyResultDataAccessException e) {
	            return null;
	        }
	    }
}
