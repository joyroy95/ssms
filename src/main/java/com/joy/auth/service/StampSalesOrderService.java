package com.joy.auth.service;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.joy.auth.common.ListVo;
import com.joy.auth.vo.SsmCategoryVo;
import com.joy.auth.vo.SsmInventoryVo;
import com.joy.auth.vo.StampOrderVo;

@Service
public class StampSalesOrderService {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
    
	 /**
     ****This method is used to load inventory list of SSM.
     ****@author Joy Roy <joyroylightoj@gmail.com>
     **/

    public List<ListVo> loadInventoryListForDropDown(String userId) throws SQLException {
       List<ListVo> inventoryList = new ArrayList<>();
       ListVo listVo;
        String sqlQuery = "SELECT S.INV_ID,\r\n"
        		+ "          (SELECT D.DISTRICT_NAME\r\n"
        		+ "             FROM SSM_DISTRICT D\r\n"
        		+ "            WHERE D.DISTRICT_ID = S.DISTRICT_ID)\r\n"
        		+ "       || ' - '\r\n"
        		+ "       || (SELECT C.CATEGORY_NAME\r\n"
        		+ "             FROM SSM_CATEGORY C\r\n"
        		+ "            WHERE C.CATEGORY_ID = S.CATEGORY_ID)    INVENTORY_NAME\r\n"
        		+ "  FROM SSD_STOCK_INV S WHERE S.DISTRICT_ID   = (SELECT u.USER_SELLING_AREA\r\n"
        		+ "                          FROM user_master u\r\n"
        		+ "                         WHERE u.USER_ID = '"+ userId + "'" + ") ORDER BY S.INV_ID";
        System.out.println("Sql query for inventoryList: " + sqlQuery);
        Connection connection = jdbcTemplate.getDataSource().getConnection();
        PreparedStatement s=connection.prepareStatement(sqlQuery);      //creating statement
        ResultSet rs=s.executeQuery();   //executing statement
        
        System.out.println(rs.getFetchSize());
        if(rs.getFetchSize()>0)
        {
            while(rs.next()){
                listVo = new ListVo();
                System.out.println(rs.getString(1));
                System.out.println(rs.getString(2));
                listVo.setListKey(rs.getString(1));
                listVo.setListValue(rs.getString(2));
                inventoryList.add(listVo);
            }
        }
        connection.close();
        return inventoryList;
    }

	 /**
     ****This method is used to save stamp order info.
     ****@author Joy Roy <joyroylightoj@gmail.com>
     **/

	public Map<String, String> executeStampOrderSaveSp(StampOrderVo stampOrderVo, String userId) throws SQLException {
		Map<String, String> out = new HashMap<>();
		final String STORE_PROCEDURE_NAME = "{call PKG_SSM.SP_STAMP_ORDER(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
		Connection connection = jdbcTemplate.getDataSource().getConnection();
		CallableStatement callableStatement = connection.prepareCall(STORE_PROCEDURE_NAME);

		if (stampOrderVo.getActivityType().isEmpty() || stampOrderVo.getActivityType()=="") {
			stampOrderVo.setActivityType("I");
		}

		callableStatement.setString(1, stampOrderVo.getActivityType());
		callableStatement.setString(2, stampOrderVo.getSal_id());
		callableStatement.setString(3, stampOrderVo.getDistrict_id());
		callableStatement.setString(4, stampOrderVo.getCategory_id());
		callableStatement.setString(5, stampOrderVo.getSale_amt());
		callableStatement.setString(6, stampOrderVo.getSale_fee());
		callableStatement.setString(7, stampOrderVo.getSale_total_amt());
		callableStatement.setString(8, stampOrderVo.getInv_id());
		callableStatement.setString(9, "");
		callableStatement.setString(10, "");
		callableStatement.setString(11, "");
		callableStatement.setString(12, userId);
		callableStatement.registerOutParameter(13, Types.VARCHAR);
		callableStatement.registerOutParameter(14, Types.VARCHAR);
		callableStatement.registerOutParameter(15, Types.VARCHAR);

		callableStatement.execute();
		out.put("P_OUT", callableStatement.getString(13));
		out.put("P_ERROR_CODE", callableStatement.getString(14));
		out.put("P_ERROR_MESSAGE", callableStatement.getString(15));
		connection.close();
		out.forEach((k, v) -> System.out.println("STamp Order Save Sp: " + k + " " + v));
		

		return out;

	}
	
	 /**
     ****This method is used to load inventory list of SSM.
     ****@author Joy Roy <joyroylightoj@gmail.com>
     **/

    public StampOrderVo loadStampOrderBySalId(String sal_id) throws SQLException {
       
       StampOrderVo stampOrderVo = new StampOrderVo();
        String sqlQuery = "select s.SAL_ID, s.DISTRICT_ID, s.CATEGORY_ID, s.inv_id, s.SALE_AMT, s.SALE_FEE, s.SALE_TOTAL_AMT from ssmd_sales s where s.SAL_ID = " + sal_id;
        Connection connection = jdbcTemplate.getDataSource().getConnection();
        PreparedStatement s=connection.prepareStatement(sqlQuery);      //creating statement
        ResultSet rs=s.executeQuery();   //executing statement
        
        System.out.println(rs.getFetchSize());
        if(rs.getFetchSize()>0)
        {
        	while(rs.next())
        	{
            stampOrderVo.setSal_id(rs.getString(1));
            stampOrderVo.setDistrict_id(rs.getString(2));
            stampOrderVo.setCategory_id(rs.getString(3));
            stampOrderVo.setInv_id(rs.getString(4));
            stampOrderVo.setSale_amt(rs.getString(5));
            stampOrderVo.setSale_fee(rs.getString(6));
            stampOrderVo.setSale_total_amt(rs.getString(7));
        	}
        }
        System.out.println("stampOrderVo from loadStampOrderBySalId: " + stampOrderVo);
        connection.close();
        return stampOrderVo;
    }
    
	 /**
     ****This method is used to authorize stamp order info.
     ****@author Joy Roy <joyroylightoj@gmail.com>
     **/

	public Map<String, String> authorizeStampOrderSp(StampOrderVo stampOrderVo, String userId) throws SQLException {
		Map<String, String> out = new HashMap<>();
		final String STORE_PROCEDURE_NAME = "{call PKG_SSM.SP_STAMP_ORDER(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
		Connection connection = jdbcTemplate.getDataSource().getConnection();
		CallableStatement callableStatement = connection.prepareCall(STORE_PROCEDURE_NAME);

		

		callableStatement.setString(1, "U");
		callableStatement.setString(2, stampOrderVo.getSal_id());
		callableStatement.setString(3, stampOrderVo.getDistrict_id());
		callableStatement.setString(4, stampOrderVo.getCategory_id());
		callableStatement.setString(5, stampOrderVo.getSale_amt());
		callableStatement.setString(6, stampOrderVo.getSale_fee());
		callableStatement.setString(7, stampOrderVo.getSale_total_amt());
		callableStatement.setString(8, stampOrderVo.getInv_id());
		callableStatement.setString(9, stampOrderVo.getOrder_status());
		callableStatement.setDate(10, stampOrderVo.getStamp_receive_date());
		callableStatement.setString(11, stampOrderVo.getChalan_no());
		callableStatement.setString(12, userId);
		callableStatement.registerOutParameter(13, Types.VARCHAR);
		callableStatement.registerOutParameter(14, Types.VARCHAR);
		callableStatement.registerOutParameter(15, Types.VARCHAR);

		callableStatement.execute();
		out.put("P_OUT", callableStatement.getString(13));
		out.put("P_ERROR_CODE", callableStatement.getString(14));
		out.put("P_ERROR_MESSAGE", callableStatement.getString(15));
		connection.close();
		out.forEach((k, v) -> System.out.println("STamp Order Authorize Save Sp: " + k + " " + v));
		

		return out;

	}
	
	 /**
     ****This method is used to get District id by user_id.
     ****@author Joy Roy <joyroylightoj@gmail.com>
     **/

    public StampOrderVo getDistrictIdByUserId(String userId) throws SQLException {
       
       StampOrderVo stampOrderVo = new StampOrderVo();
        String sqlQuery = "select us.USER_SELLING_AREA from user_master us where user_id  = " + "'" + userId + "'";
        Connection connection = jdbcTemplate.getDataSource().getConnection();
        PreparedStatement s=connection.prepareStatement(sqlQuery);      //creating statement
        ResultSet rs=s.executeQuery();   //executing statement
        
        System.out.println(rs.getFetchSize());
        if(rs.getFetchSize()>0)
        {
        	while(rs.next())
        	{
            stampOrderVo.setDistrict_id(rs.getString(1));
        	}
        }
        System.out.println("stampOrderVo from getDistrictIdByUserId: " + stampOrderVo);
        connection.close();
        return stampOrderVo;
    }
    
	 /**
     ****This method is used to get District id by user_id.
     ****@author Joy Roy <joyroylightoj@gmail.com>
     **/
    public StampOrderVo getInventoryByCategory(String categoryId, String districtId) throws SQLException {
        
        StampOrderVo stampOrderVo = new StampOrderVo();
         String sqlQuery = "SELECT S.INV_ID FROM SSD_STOCK_INV S WHERE S.CATEGORY_ID   =  " + categoryId + " AND DISTRICT_ID = " + districtId;
         System.out.println("Sql query for getInventoryByCategory: " + sqlQuery);
         Connection connection = jdbcTemplate.getDataSource().getConnection();
         PreparedStatement s=connection.prepareStatement(sqlQuery);      //creating statement
         ResultSet rs=s.executeQuery();   //executing statement
         
         System.out.println(rs.getFetchSize());
         if(rs.getFetchSize()>0)
         {
         	while(rs.next())
         	{
             stampOrderVo.setInv_id(rs.getString(1));
         	}
         }
         System.out.println("stampOrderVo from getInventoryByCategory: " + stampOrderVo);
         connection.close();
         return stampOrderVo;
     }


}
