package com.joy.auth.service;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.joy.auth.common.ListVo;
import com.joy.auth.vo.SsmCategoryVo;
import com.joy.auth.vo.SsmInventoryVo;
import com.joy.auth.vo.StampOrderVo;

@Service
public class MenuMasterService {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
    
	 /**
     ****This method is used to load menu list by role .
     ****@author Joy Roy <joyroylightoj@gmail.com>
     **/

    public List<ListVo> loadMenuListByRole(String roleName) throws SQLException {
       List<ListVo> menuList = new ArrayList<>();
       ListVo listVo;
        String sqlQuery = "SELECT M.MENU_NAME, TRIM (M.MENU_URL) MENU_URL\r\n"
        		+ "  FROM MENU_MASTER M\r\n"
        		+ " WHERE M.MENU_ID IN (SELECT R.MENU_ID\r\n"
        		+ "                       FROM ROLE_MENU_MAP R\r\n"
        		+ "                      WHERE R.ROLE_ID = (SELECT A.ROLE_ID\r\n"
        		+ "                                           FROM APP_ROLE A\r\n"
        		+ "                                          WHERE A.ROLE_NAME = '"+roleName+"'" + ")) ORDER BY MENU_ID";
        System.out.println("Sql query for menuList: " + sqlQuery);
        Connection connection = jdbcTemplate.getDataSource().getConnection();
        PreparedStatement s=connection.prepareStatement(sqlQuery);      //creating statement
        ResultSet rs=s.executeQuery();   //executing statement
        
        System.out.println(rs.getFetchSize());
        if(rs.getFetchSize()>0)
        {
            while(rs.next()){
                listVo = new ListVo();
                System.out.println(rs.getString(1));
                System.out.println(rs.getString(2));
                listVo.setListKey(rs.getString(1));
                listVo.setListValue(rs.getString(2));
                menuList.add(listVo);
            }
        }
        connection.close();
        return menuList;
    }
}
