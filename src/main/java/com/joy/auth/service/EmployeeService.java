package com.joy.auth.service;

import javax.naming.NameNotFoundException;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.joy.auth.dao.EmployeeDao;
import com.joy.auth.mapper.EmployeeDaoMapper;
import com.joy.auth.model.Employees;

import javassist.NotFoundException;

@Service
public class EmployeeService{
	
	@Autowired
	private JdbcTemplate jdbcTemplateObject;
	
	@Autowired
	private EmployeeDao employeeDao;
	
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplateObject = new JdbcTemplate(dataSource);
	}


	public void checkEmployeeId(int Employee_Id) throws NotFoundException{

		Employees employee = this.employeeDao.findEmployeeAccount(Employee_Id);
		
		if(employee == null)
		{
			throw new NotFoundException("Employee Not found in Database");
		}
		
		System.out.println("checkEmployeeId: "+ employee.getEmployee_Id()+ employee.getFirst_Name()+employee.getLast_Name());
		
	}

}
