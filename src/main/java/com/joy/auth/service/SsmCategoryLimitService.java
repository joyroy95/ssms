package com.joy.auth.service;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.joy.auth.common.ListVo;
import com.joy.auth.vo.SsmCategoryLimitVo;
import com.joy.auth.vo.SsmCategoryVo;
import com.joy.auth.vo.SsmInventoryVo;

@Service
public class SsmCategoryLimitService {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	 /**
     ****This method is used to save ssm Category limit info.
     ****@author Joy Roy <joyroylightoj@gmail.com>
     **/

	public Map<String, String> executeCategoryLimitSaveSp(SsmCategoryLimitVo ssmCategoryLimitVo, String userId) throws SQLException {
		Map<String, String> out = new HashMap<>();
		final String STORE_PROCEDURE_NAME = "{call PKG_SSM.SP_SSM_CATEGORY_LIMIT(?,?,?,?,?,?,?,?)}";
		Connection connection = jdbcTemplate.getDataSource().getConnection();
		CallableStatement callableStatement = connection.prepareCall(STORE_PROCEDURE_NAME);

		if (ssmCategoryLimitVo.getActivityType().isEmpty() || ssmCategoryLimitVo.getActivityType()=="") {
			ssmCategoryLimitVo.setActivityType("I");
		}

		callableStatement.setString(1, ssmCategoryLimitVo.getActivityType());
		callableStatement.setString(2, ssmCategoryLimitVo.getDistrict_id());
		callableStatement.setString(3, ssmCategoryLimitVo.getCategory_id());
		callableStatement.setString(4, ssmCategoryLimitVo.getLimit_cnt());
		callableStatement.setString(5, userId);
		callableStatement.registerOutParameter(6, Types.VARCHAR);
		callableStatement.registerOutParameter(7, Types.VARCHAR);
		callableStatement.registerOutParameter(8, Types.VARCHAR);

		callableStatement.execute();
		out.put("P_OUT", callableStatement.getString(6));
		out.put("P_ERROR_CODE", callableStatement.getString(7));
		out.put("P_ERROR_MESSAGE", callableStatement.getString(8));
		connection.close();
		out.forEach((k, v) -> System.out.println("SSM Category Limit Save Sp: " + k + " " + v));
		

		return out;

	}
}
