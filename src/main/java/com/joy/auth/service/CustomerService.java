package com.joy.auth.service;

import java.sql.Timestamp;
import java.sql.Types;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.joy.auth.dao.AppUserDAO;
import com.joy.auth.model.Customer;

import aj.org.objectweb.asm.Type;

@Service
public class CustomerService {
	
	@Autowired
	private JdbcTemplate jdbcTemplateObject;
	
	@Autowired
	private AppUserDAO appUserDAO;
	
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplateObject = new JdbcTemplate(dataSource);
	}
	
	
	public void insetIntoCustomer(Customer customer, String userName)
	{
		String userId = this.appUserDAO.findUserAccount(userName).getUserId();
			String insertSql =

					"INSERT INTO CUSTOMER (" +
							" CUSTOMER_NAME, " +
							" FATHERS_NAME, " +
							" MOTHERS_NAME, " +
							" DATEOFBIRTH, " +
							" ADDRESS, " +
							" PHONE_NO, " +
							" NID_NO, " +
							" BRANCH_CODE, "+
							" USER_ID)" +

							"VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

			Object[] params = new Object[] { customer.getCustomerName(), customer.getFathersName(), customer.getMothersName()
					, customer.getDateOfBirth(), customer.getAddress(), customer.getPhoneNumber(), customer.getNidNumber(), customer.getBranchCode(),userId};

			// define SQL types of the arguments
			int[] types = new int[] { Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.DATE, Types.VARCHAR,Types.VARCHAR,Types.VARCHAR,Types.NUMERIC, Types.NUMERIC};

			try {
				int row = jdbcTemplateObject.update(insertSql, params, types);
				System.out.println(row + " row inserted.");
			} catch (DuplicateKeyException e) {
				throw new DuplicateKeyException("Nid is already exists");
			}
	}
	
	public String createAccount(Customer customer)
	{
		String sql1 = "Select CUSTOMER_ID From CUSTOMER " + " where NID_NO = ? ";
		System.out.println("From create account ");
		int customer_id = jdbcTemplateObject.queryForObject(sql1, new Object[]{customer.getNidNumber()}, Integer.class);
		System.out.println("From create account "+ customer_id);
		
		String sql2 = "Select ACCTYPE_ID From ACCOUNT_TYPE " + " where ACCTYPE_NAME = ? ";
		
		int ACCTYPE_ID = jdbcTemplateObject.queryForObject(sql2, new Object[]{customer.getAccountType()}, Integer.class);
		
		String accountNo = Integer.toString(customer.getBranchCode()) + "." + Integer.toString(ACCTYPE_ID) + "." + Integer.toString(customer_id);
		
		String insertSql =
				"INSERT INTO ACCOUNT (" +
						" ACCOUNT_NO, " +
						" ACCTYPE_ID, " +
						" CUSTOMER_ID , " +
						" ACCOUNT_BALANCE, " +
						" OPEN_DATE)" +
						"VALUES (?, ?, ?, ?, ?)";
		
		Object[] params = new Object[] { accountNo, ACCTYPE_ID, customer_id, 0, new Date()};
		
		int[] types = new int[] { Types.VARCHAR, Types.NUMERIC, Types.NUMERIC, Types.NUMERIC, Types.DATE};
		int row = jdbcTemplateObject.update(insertSql, params, types);
		System.out.println(row + " row inserted in Accounts table");
		return accountNo;
	}
	
	public void checkAccountNumber(String accountNo)
	{
		String sql = "Select CUSTOMER_ID From ACCOUNT " + " where ACCOUNT_NO = ? ";
		try {
			int customer_id = jdbcTemplateObject.queryForObject(sql, new Object[]{accountNo}, Integer.class);
			System.out.println(customer_id);
		} catch (EmptyResultDataAccessException e) {
			throw new EmptyResultDataAccessException(2);
		}
			
	}
	
	public void insertIntoTransactionHistoryCredit(String accountNo, int amount)
	{
		String insertSql =
				"INSERT INTO TRANSACTION_HISTORY (" +
						" ACCOUNT_NO, " +
						" TRANSTYPE_NAME, " +
						" TRANS_POSTDATE, " +
						" TRANS_AMOUNT," +
						" TOTAL_BALANCE)" +
						"VALUES (?, ?, ?, ?, ?)";
		String sql = "Select ACCOUNT_BALANCE From ACCOUNT " + " where ACCOUNT_NO = ? ";
		int netAccountBalance = jdbcTemplateObject.queryForObject(sql, new Object[]{accountNo}, Integer.class);
		System.out.println("netAccountBalance: " + netAccountBalance);
		Object[] params = new Object[] { accountNo, "Credit", new Timestamp(new Date().getTime()), amount, netAccountBalance+amount};
		int[] types = new int[] { Types.VARCHAR, Types.VARCHAR, Types.TIMESTAMP, Types.NUMERIC, Types.NUMERIC};
		int row = jdbcTemplateObject.update(insertSql, params, types);
		System.out.println(row + " row inserted in Accounts table");
		
		String SQL = "update  ACCOUNT set ACCOUNT_BALANCE = ? where ACCOUNT_NO = ?";

		jdbcTemplateObject.update(SQL, netAccountBalance+amount, accountNo);

		System.out.println("Updated Record with ID = " + accountNo);
	}
	
	public boolean insertIntoTransactionHistoryDebit(String accountNo, int amount)
	{
		String insertSql =
				"INSERT INTO TRANSACTION_HISTORY (" +
						" ACCOUNT_NO, " +
						" TRANSTYPE_NAME, " +
						" TRANS_POSTDATE, " +
						" TRANS_AMOUNT," +
						" TOTAL_BALANCE)" +
						"VALUES (?, ?, ?, ?, ?)";
		String sql = "Select ACCOUNT_BALANCE From ACCOUNT " + " where ACCOUNT_NO = ? ";
		int netAccountBalance = jdbcTemplateObject.queryForObject(sql, new Object[]{accountNo}, Integer.class);
		if(netAccountBalance>=amount)
		{
			System.out.println("netAccountBalance: " + netAccountBalance);
			Object[] params = new Object[] { accountNo, "Credit", new Timestamp(new Date().getTime()), amount, netAccountBalance-amount};
			int[] types = new int[] { Types.VARCHAR, Types.VARCHAR, Types.TIMESTAMP, Types.NUMERIC, Types.NUMERIC};
			int row = jdbcTemplateObject.update(insertSql, params, types);
			System.out.println(row + " row inserted in Accounts table");
			
			String SQL = "update  ACCOUNT set ACCOUNT_BALANCE = ? where ACCOUNT_NO = ?";

			jdbcTemplateObject.update(SQL, netAccountBalance-amount, accountNo);

			System.out.println("Updated Record with ID = " + accountNo);
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public Map<String, Object> showInformation(String accountNo)
	{
		String sql = "Select * From ACCOUNT " + " where ACCOUNT_NO = ? ";
		
		List<Map<String, Object>> account =jdbcTemplateObject.queryForList(sql, accountNo);
		
		int customerId = 0;
		Object object;
		
		Map<String, Object> map = new HashedMap();
		
		for(Map<String, Object> r: account) {
			//System.out.println(r.get("CUSTOMER_ID"));
			object = r.get("CUSTOMER_ID");
			customerId = Integer.valueOf(object.toString());
			map.put("Account Balance", r.get("ACCOUNT_BALANCE"));
			map.put("Account No", r.get("ACCOUNT_NO"));
			map.put("Account Open date", r.get("OPEN_DATE"));
			map.put("Account TYPE ID", r.get("ACCTYPE_ID"));
			}
		
		String sql1 = "Select * From CUSTOMER " + " where CUSTOMER_ID = ? ";
		List<Map<String, Object>> customerInfo =jdbcTemplateObject.queryForList(sql1, customerId);
		
		
		for(Map<String, Object> r: customerInfo) {
			map.put("Customer Name", r.get("CUSTOMER_NAME"));
			map.put("Fathers Name", r.get("FATHERS_NAME"));
			map.put("Mothers Name", r.get("MOTHERS_NAME"));
			map.put("Date Of Birth", r.get("DATEOFBIRTH"));
			map.put("Address", r.get("ADDRESS"));
			map.put("Phone No", r.get("PHONE_NO"));
			map.put("NID NO", r.get("NID_NO"));
			map.put("Branch Code", r.get("BRANCH_CODE"));
			}
	
		
		return map;
		
	}

}
