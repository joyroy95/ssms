package com.joy.auth.service;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.joy.auth.common.ListVo;
import com.joy.auth.vo.SsmCategoryVo;
import com.joy.auth.vo.SsmInventoryVo;

@Service
public class SsmInventoryService {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	 /**
     ****This method is used to load district list of SSM.
     ****@author Joy Roy <joyroylightoj@gmail.com>
     **/

    public List<ListVo> loadDistrictListForDropDown() throws SQLException {
       List<ListVo> districtList = new ArrayList<>();
       ListVo listVo;
        String sqlQuery = "SELECT D.DISTRICT_ID, D.DISTRICT_NAME FROM SSM_DISTRICT D ORDER BY D.DISTRICT_NAME ";
        Connection connection = jdbcTemplate.getDataSource().getConnection();
        PreparedStatement s=connection.prepareStatement(sqlQuery);      //creating statement
        ResultSet rs=s.executeQuery();   //executing statement
        
        System.out.println(rs.getFetchSize());
        if(rs.getFetchSize()>0)
        {
            while(rs.next()){
                listVo = new ListVo();
                System.out.println(rs.getString(1));
                System.out.println(rs.getString(2));
                listVo.setListKey(rs.getString(1));
                listVo.setListValue(rs.getString(2));
                districtList.add(listVo);
            }
        }
        connection.close();
        return districtList;
    }
    
	 /**
     ****This method is used to load Category list of SSM.
     ****@author Joy Roy <joyroylightoj@gmail.com>
     ****IT Development Division
     **/

    public List<ListVo> loadCategoryListForDropDown() throws SQLException {
       List<ListVo> categoryList = new ArrayList<>();
       ListVo listVo;
        String sqlQuery = "select C.CATEGORY_ID, C.CATEGORY_NAME from SSM_CATEGORY C ORDER BY CATEGORY_NAME";
        Connection connection = jdbcTemplate.getDataSource().getConnection();
        PreparedStatement s=connection.prepareStatement(sqlQuery);      //creating statement
        ResultSet rs=s.executeQuery();   //executing statement
        
        System.out.println(rs.getFetchSize());
        if(rs.getFetchSize()>0)
        {
            while(rs.next()){
                listVo = new ListVo();
                System.out.println(rs.getString(1));
                System.out.println(rs.getString(2));
                listVo.setListKey(rs.getString(1));
                listVo.setListValue(rs.getString(2));
                categoryList.add(listVo);
            }
        }
        connection.close();
        return categoryList;
    }

	 /**
     ****This method is used to save ssm inventory info.
     ****@author Joy Roy <joyroylightoj@gmail.com>
     **/

	public Map<String, String> executeInventorySaveSp(SsmInventoryVo ssmInventoryVo, String userId) throws SQLException {
		Map<String, String> out = new HashMap<>();
		final String STORE_PROCEDURE_NAME = "{call PKG_SSM.SP_SSM_INVENTORY(?,?,?,?,?,?,?,?,?)}";
		Connection connection = jdbcTemplate.getDataSource().getConnection();
		CallableStatement callableStatement = connection.prepareCall(STORE_PROCEDURE_NAME);

		if (ssmInventoryVo.getActivityType().isEmpty() || ssmInventoryVo.getActivityType()=="") {
			ssmInventoryVo.setActivityType("I");
		}

		callableStatement.setString(1, ssmInventoryVo.getActivityType());
		callableStatement.setString(2, ssmInventoryVo.getInv_id());
		callableStatement.setString(3, ssmInventoryVo.getDistrict_id());
		callableStatement.setString(4, ssmInventoryVo.getCategory_id());
		callableStatement.setString(5, ssmInventoryVo.getStock_cnt());
		callableStatement.setString(6, userId);
		callableStatement.registerOutParameter(7, Types.VARCHAR);
		callableStatement.registerOutParameter(8, Types.VARCHAR);
		callableStatement.registerOutParameter(9, Types.VARCHAR);

		callableStatement.execute();
		out.put("P_OUT", callableStatement.getString(7));
		out.put("P_ERROR_CODE", callableStatement.getString(8));
		out.put("P_ERROR_MESSAGE", callableStatement.getString(9));
		connection.close();
		out.forEach((k, v) -> System.out.println("SSM Inventory Save Sp: " + k + " " + v));
		

		return out;

	}
	
	 /**
     ****This method is used to load district list of SSM.
     ****@author Joy Roy <joyroylightoj@gmail.com>
     **/

    public List<ListVo> loadInventoryDashBoardList(String userId) throws SQLException {
       List<ListVo> inventoryDashBoardList = new ArrayList<>();
       ListVo listVo;
       String sqlQuery = "";
       if(userId==null || userId=="" || userId.isEmpty())
       {
    	   
    	   sqlQuery = "SELECT \r\n"
           		+ "       (SELECT C.CATEGORY_NAME\r\n"
           		+ "          FROM SSM_CATEGORY C\r\n"
           		+ "         WHERE C.CATEGORY_ID = I.CATEGORY_ID)\r\n"
           		+ "           CATEGORY_NAME,\r\n"
           		+ "         I.STOCK_CNT\r\n"
           		+ "       - NVL((SELECT SUM (S.SALE_AMT)\r\n"
           		+ "            FROM SSMD_SALES S\r\n"
           		+ "           WHERE     S.inv_id = I.inv_id\r\n"
           		+ "                 AND S.ORDER_STATUS = 'P'\r\n"
           		+ "                 AND S.AUTH_STATUS = 'U'), 0) Remaining_Stock\r\n"
           		+ "  FROM SSD_STOCK_INV I\r\n"
           		+ " WHERE I.DISTRICT_ID = 17 AND AUTH_STATUS = 'A' ORDER BY CATEGORY_NAME";

       }
       else
       {
         sqlQuery = "SELECT \r\n"
        		+ "       (SELECT C.CATEGORY_NAME\r\n"
        		+ "          FROM SSM_CATEGORY C\r\n"
        		+ "         WHERE C.CATEGORY_ID = I.CATEGORY_ID)\r\n"
        		+ "           CATEGORY_NAME,\r\n"
        		+ "         I.STOCK_CNT\r\n"
        		+ "       - NVL((SELECT SUM (S.SALE_AMT)\r\n"
        		+ "            FROM SSMD_SALES S\r\n"
        		+ "           WHERE     S.inv_id = I.inv_id\r\n"
        		+ "                 AND S.ORDER_STATUS = 'P'\r\n"
        		+ "                 AND S.AUTH_STATUS = 'U'), 0) Remaining_Stock\r\n"
        		+ "  FROM SSD_STOCK_INV I\r\n"
        		+ " WHERE I.DISTRICT_ID = (SELECT u.USER_SELLING_AREA\r\n"
        		+ "                              FROM user_master u\r\n"
        		+ "                             WHERE u.USER_ID = '" + userId + "'" + " ) AND AUTH_STATUS = 'A' ORDER BY CATEGORY_NAME";
        
       }
        System.out.println("sqlQuery for inventoryDashBoardList: " + sqlQuery);
        Connection connection = jdbcTemplate.getDataSource().getConnection();
        PreparedStatement s=connection.prepareStatement(sqlQuery);      //creating statement
        ResultSet rs=s.executeQuery();   //executing statement
        
        System.out.println(rs.getFetchSize());
        if(rs.getFetchSize()>0)
        {
            while(rs.next()){
                listVo = new ListVo();
                listVo.setListKey(rs.getString(1));
                listVo.setListValue(rs.getString(2));
                inventoryDashBoardList.add(listVo);
            }
        }
        
        connection.close();
        return inventoryDashBoardList;
    }
}
