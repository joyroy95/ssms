package com.joy.auth.service;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.joy.auth.common.ListVo;
import com.joy.auth.domain.StampOrderOfficialReportModel;
import com.joy.auth.vo.SsmCategoryVo;
import com.joy.auth.vo.SsmInventoryVo;
import com.joy.auth.vo.StampOrderVo;

@Service
public class StampSalesOrderReportService {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
    
	 /**
     ****This method is used to get all stamp order for official by date.
     ****@author Joy Roy <joyroylightoj@gmail.com>
     **/

    public List<StampOrderOfficialReportModel> getAllStampOrderForOfficialByDate(String userId) throws SQLException {
       List<StampOrderOfficialReportModel> stampOrderListForOfficial = new ArrayList<>();
       StampOrderOfficialReportModel stampOrderOfficialReportModel;
        String sqlQuery = "SELECT ROWNUM AS SL_NO,\r\n"
				+ "       (SELECT d.DISTRICT_NAME\r\n"
				+ "          FROM SSM_DISTRICT d\r\n"
				+ "         WHERE d.DISTRICT_ID = S.DISTRICT_ID)    DISTRICT_NAME,\r\n"
				+ "        (SELECT C.CATEGORY_NAME\r\n"
				+ "          FROM SSM_CATEGORY C\r\n"
				+ "         WHERE C.CATEGORY_ID = S.CATEGORY_ID)    CATEGORY_NAME,\r\n"
				+ "         SALE_AMT,\r\n"
				+ "         SALE_FEE,\r\n"
				+ "         S.SALE_TOTAL_AMT,\r\n"
				+ "         CHALAN_NO,\r\n"
				+ "         TO_CHAR(STAMP_RECEIVE_DATE, 'dd/MM/yyyy') STAMP_RECEIVE_DATE,\r\n"
				+ "         ORDER_BY,\r\n"
				+ "         TO_CHAR(ORDER_DATE, 'dd/MM/yyyy') ORDER_DATE,\r\n"
				+ "         SALE_BY,\r\n"
				+ "         TO_CHAR(SALE_DATE, 'dd/MM/yyyy') SALE_DATE,\r\n"
				+ "         CASE WHEN ORDER_STATUS = 'D'\r\n"
				+ "         THEN 'Declined'\r\n"
				+ "         WHEN ORDER_STATUS = 'A'\r\n"
				+ "         THEN 'Approved'\r\n"
				+ "         ELSE 'UnAuthorized'\r\n"
				+ "         END ORDER_STATUS\r\n"
				+ "         \r\n"
				+ "  FROM SSMD_SALES S WHERE s.DISTRICT_ID = (SELECT u.USER_SELLING_AREA\r\n"
				+ "				                             FROM user_master u\r\n"
				+ "				                           WHERE u.USER_ID = '" + userId + "'" + " )";
        System.out.println("Sql query for stampOrderListForOfficial: " + sqlQuery);
        Connection connection = jdbcTemplate.getDataSource().getConnection();
        PreparedStatement s=connection.prepareStatement(sqlQuery);      //creating statement
        ResultSet rs=s.executeQuery();   //executing statement
        
        System.out.println(rs.getFetchSize());
        if(rs.getFetchSize()>0)
        {
            while(rs.next()){
            	stampOrderOfficialReportModel = new StampOrderOfficialReportModel();
            	stampOrderOfficialReportModel.setSl_no(rs.getString(1));
            	stampOrderOfficialReportModel.setDistrict_name(rs.getString(2));
            	stampOrderOfficialReportModel.setCategory_name(rs.getString(3));
            	stampOrderOfficialReportModel.setSale_amt(rs.getString(4));
            	stampOrderOfficialReportModel.setSale_fee(rs.getString(5));
            	stampOrderOfficialReportModel.setSale_total_amt(rs.getString(6));
            	stampOrderOfficialReportModel.setChalan_no(rs.getString(7));
            	stampOrderOfficialReportModel.setStamp_receive_date(rs.getString(8));
            	stampOrderOfficialReportModel.setOrder_by(rs.getString(9));
            	stampOrderOfficialReportModel.setOrder_date(rs.getString(10));
            	stampOrderOfficialReportModel.setOrder_status(rs.getString(11));
                stampOrderListForOfficial.add(stampOrderOfficialReportModel);
            }
        }
        connection.close();
        return stampOrderListForOfficial;
    }

}
