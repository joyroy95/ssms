package com.joy.auth.service;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.joy.auth.vo.SsmCategoryVo;

@Service
public class SsmCategoryService {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	public Map<String, String> executeCategorySaveSp(SsmCategoryVo ssmCategoryVo, String userId) throws SQLException {
		Map<String, String> out = new HashMap<>();
		final String STORE_PROCEDURE_NAME = "{call PKG_SSM.SP_SSM_CATEGORY(?,?,?,?,?,?,?,?)}";
		Connection connection = jdbcTemplate.getDataSource().getConnection();
		CallableStatement callableStatement = connection.prepareCall(STORE_PROCEDURE_NAME);

		if (ssmCategoryVo.getActivityType().isEmpty() || ssmCategoryVo.getActivityType()=="") {
			ssmCategoryVo.setActivityType("I");
		}

		callableStatement.setString(1, ssmCategoryVo.getActivityType());
		callableStatement.setString(2, ssmCategoryVo.getCategory_id());
		callableStatement.setString(3, ssmCategoryVo.getCategory_name());
		callableStatement.setString(4, ssmCategoryVo.getCategory_description());
		callableStatement.setString(5, userId);
		callableStatement.registerOutParameter(6, Types.VARCHAR);
		callableStatement.registerOutParameter(7, Types.VARCHAR);
		callableStatement.registerOutParameter(8, Types.VARCHAR);

		callableStatement.execute();
		out.put("P_OUT", callableStatement.getString(6));
		out.put("P_ERROR_CODE", callableStatement.getString(7));
		out.put("P_ERROR_MESSAGE", callableStatement.getString(8));

		out.forEach((k, v) -> System.out.println("SSM Category Save Sp: " + k + " " + v));
		connection.close();

		return out;

	}
}
