package com.joy.auth.service;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.OptionalLong;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.util.TypeKey;
import com.joy.auth.dao.AppRoleDAO;
import com.joy.auth.dao.AppUserDAO;
import com.joy.auth.dao.UserRoleDao;
import com.joy.auth.domain.StampOrderOfficialReportModel;
import com.joy.auth.model.AppUser;
import com.joy.auth.model.UserRole;
import com.joy.auth.utils.GeneratePassword;
import com.joy.auth.vo.SsmInventoryVo;
import com.joy.auth.vo.StampOrderVo;
import com.joy.auth.vo.UserMasterVo;

import javassist.NotFoundException;

@Service
public class AppUserService {

	@Autowired
	UserDetailsServiceImpl userDetailsService;
	@Autowired
	private JdbcTemplate jdbcTemplateObject;

	@Autowired
	private AppUserDAO appUserDAO;

	@Autowired
	private AppRoleDAO appRoleDAO;
	
	@Autowired
	private UserRoleDao userRoleDao;

	public boolean checkAdminCurrentPassword(String userName, String password) {

		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

		if (passwordEncoder.matches(password, userDetailsService.loadUserByUsername(userName).getPassword()))

		{
			return true;
		}

		return false;
	}

	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplateObject = new JdbcTemplate(dataSource);
	}

	public static String passwordEncoder(String password) {
		BCryptPasswordEncoder Encoder = new BCryptPasswordEncoder();

		return Encoder.encode(password);
	}

	public void updatePassword(String userName, String newPassword) {

		String encryptedPassword = passwordEncoder(newPassword);

		AppUser appUser = this.appUserDAO.findUserAccount(userName);

		String SQL = "update USER_MASTER set USER_PASSWORD = ? where User_Id = ?";

		jdbcTemplateObject.update(SQL, encryptedPassword, appUser.getUserId());

		System.out.println("Updated Record with ID = " + userName);
	}
	
	public void resetPassword(String userName, String newPassword) {

		AppUser appUser = this.appUserDAO.findUserAccount(userName);

		String SQL = "update USER_MASTER set USER_PASSWORD = ? where User_Id = ?";

		jdbcTemplateObject.update(SQL, newPassword, appUser.getUserId());

		System.out.println("Updated Record with ID = " + userName);
	}

	public void insetIntoAppUser(String userName, String encryptedPassword) throws NullPointerException {

		AppUser appUser = this.appUserDAO.findUserAccount(userName);
		String insertSql =

				"INSERT INTO App_User (" +

						" User_Name, " +

						" Encryted_Password, " +

						" Enabled)" +

						"VALUES (?, ?, ?)";

		Object[] params = new Object[] { userName, encryptedPassword, 1 };

		// define SQL types of the arguments
		int[] types = new int[] { Types.VARCHAR, Types.VARCHAR, Types.NUMERIC };

		if (appUser == null) {

			int row = jdbcTemplateObject.update(insertSql, params, types);
			System.out.println(row + " row inserted.");
		} else
			throw new NullPointerException("User id Exists");

	}

	public List<String> getRoles() {
		List<String> roles = this.appRoleDAO.getAllRoleNames();

		return roles;
	}

	public boolean assignRole(String userName, String roleName) {
		String userId = this.appUserDAO.findUserAccount(userName).getUserId();
		int roleId = this.appRoleDAO.findRoleAccount(roleName).getRoleId();
		
		String sql = "Select u.User_Id, u.Role_Id From User_Role u " + " where u.User_Id = ? ";
		
		List<Map<String, Object>> re=jdbcTemplateObject.queryForList(sql, userId);
		boolean roleidCheck = false;
		for(Map<String, Object> r: re) {
		boolean check = true;
			for(Object val: r.values())
			{
				if(check)
				{
					check = false;
				}
				else
				{
					int i = Integer.parseInt(val.toString());
					if(roleId==i)
					{
						roleidCheck = true;

					}
					
				}
					
			}
			if(roleidCheck)
			{
				break;
			}
		}
		
		if(!roleidCheck)
		{
		String insertSql =
				"INSERT INTO User_Role (" +

						" User_Id, " +

						" Role_Id )" +

						"VALUES (?, ?)";
		
		Object[] params = new Object[] { userId, roleId};

		// define SQL types of the arguments
		int[] types = new int[] { Types.VARCHAR, Types.NUMERIC };
		System.out.println("before insert");
		int row = jdbcTemplateObject.update(insertSql, params, types);
		System.out.println(row + " row inserted.");
		return true;
		}
		else return false;
	}
	
	public boolean UserIdcheck(String userName)
	{
		AppUser appUser = this.appUserDAO.findUserAccount(userName);
		if(appUser==null)
		{
			return false;
		}
		else return true;
		
	}
	
	 /**
     ****This method is used to save ssm user info.
     ****@author Joy Roy <joyroylightoj@gmail.com>
     **/

	public Map<String, String> executeUserMasterSaveSp(UserMasterVo userMasterVo) throws SQLException {
		Map<String, String> out = new HashMap<>();
		final String STORE_PROCEDURE_NAME = "{call PKG_SSM.SP_USER(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
		Connection connection = jdbcTemplateObject.getDataSource().getConnection();
		CallableStatement callableStatement = connection.prepareCall(STORE_PROCEDURE_NAME);

		
		  if (userMasterVo.getActivityType().isEmpty() || userMasterVo.getActivityType()==null) {
			  userMasterVo.setActivityType("I"); }
		 

		callableStatement.setString(1, userMasterVo.getActivityType());
		callableStatement.setString(2, userMasterVo.getUserId());
		callableStatement.setString(3, userMasterVo.getUserName());
		callableStatement.setString(4, userMasterVo.getUserType());
		callableStatement.setString(5, userMasterVo.getUserPassword());
		callableStatement.setString(6, userMasterVo.getUserMobile());
		callableStatement.setString(7, userMasterVo.getUserEmail());
		callableStatement.setString(8, userMasterVo.getUserFathersName());
		callableStatement.setString(9, userMasterVo.getUserMothersName());
		callableStatement.setString(10, userMasterVo.getUserNid());
		callableStatement.setString(11, userMasterVo.getUserPermanentAddress());
		callableStatement.setString(12, userMasterVo.getUserPresentAddress());
		callableStatement.setString(13, userMasterVo.getUserSellingArea());
		callableStatement.setString(14, userMasterVo.getUserLicenseNo());
		callableStatement.setDate(15, userMasterVo.getUserLicenseIssueDate());
		callableStatement.setString(16, userMasterVo.getUserBankSolvency());
		callableStatement.setString(17, userMasterVo.getUserStatus());
		callableStatement.registerOutParameter(18, Types.VARCHAR);
		callableStatement.registerOutParameter(19, Types.VARCHAR);
		callableStatement.registerOutParameter(20, Types.VARCHAR);

		callableStatement.execute();
		out.put("P_OUT", callableStatement.getString(18));
		out.put("P_ERROR_CODE", callableStatement.getString(19));
		out.put("P_ERROR_MESSAGE", callableStatement.getString(20));
		connection.close();
		out.forEach((k, v) -> System.out.println("SSM User Master Save Sp: " + k + " " + v));
		

		return out;

	}
	
	 /**
     ****This method is used to authorize ssm user info.
     ****@author Joy Roy <joyroylightoj@gmail.com>
     **/
	
	public int ssmUserMasterAuthorize(UserMasterVo userMasterVo, String userId, String checkerId) {

		String SQL = "update USER_MASTER set auth_status = ? , CHECKER_ID = ? , CHECKER_TIME = ?, USER_PASSWORD = ?  where User_Id = ?";

		int row = jdbcTemplateObject.update(SQL, "A", checkerId, LocalDateTime.now(),userMasterVo.getUserPassword(), userId);

		System.out.println(row + "Updated Record with User ID = " + userId);
		
		return row;
	}
	
	 /**
     ****This method is used to load Email from user.
     ****@author Joy Roy <joyroylightoj@gmail.com>
     **/

    public UserMasterVo loadUserEmailByUserId(String userId) throws SQLException {
       
       UserMasterVo userMasterVo = new UserMasterVo();
        String sqlQuery = "select u.USER_EMAIL from user_master u where u.USER_ID = '" + userId + "'";
        Connection connection = jdbcTemplateObject.getDataSource().getConnection();
        PreparedStatement s=connection.prepareStatement(sqlQuery);      //creating statement
        ResultSet rs=s.executeQuery();   //executing statement
        
        System.out.println(rs.getFetchSize());
        if(rs.getFetchSize()>0)
        {
        	while(rs.next())
        	{
        		userMasterVo.setUserEmail(rs.getString(1));
        	}
        }
        System.out.println("stampOrderVo from loadUserEmailByUserId: " + userMasterVo);
        connection.close();
        return userMasterVo;
    }
    
    /**
     ****This method is used to load District from user.
     ****@author Joy Roy <joyroylightoj@gmail.com>
     **/

    public UserMasterVo loadUserDistrictByUserId(String userId) throws SQLException {
       
       UserMasterVo userMasterVo = new UserMasterVo();
        String sqlQuery = "select (SELECT d.district_name FROM SSM_DISTRICT d where d.DISTRICT_ID = u.USER_SELLING_AREA) USER_SELLING_AREA from user_master u where u.USER_ID = '" + userId + "'";
        Connection connection = jdbcTemplateObject.getDataSource().getConnection();
        PreparedStatement s=connection.prepareStatement(sqlQuery);      //creating statement
        ResultSet rs=s.executeQuery();   //executing statement
        
        System.out.println(rs.getFetchSize());
        if(rs.getFetchSize()>0)
        {
        	while(rs.next())
        	{
        		userMasterVo.setUserSellingArea(rs.getString(1));
        	}
        }
        System.out.println("stampOrderVo from loadUserDistrictByUserId: " + userMasterVo);
        connection.close();
        return userMasterVo;
    }
    
    
	 /**
     ****This method is used to get all UserInfo by userId.
     ****@author Joy Roy <joyroylightoj@gmail.com>
     **/

    public UserMasterVo getAllUserInfoByUserId(String userId) throws SQLException {
       UserMasterVo userMasterVo = new UserMasterVo();
        String sqlQuery = "SELECT u.USER_ID,\r\n"
        		+ "       u.USER_NAME,\r\n"
        		+ "       u.USER_TYPE,\r\n"
        		+ "       u.USER_MOB,\r\n"
        		+ "       u.USER_EMAIL,\r\n"
        		+ "       u.USER_Father_name,\r\n"
        		+ "       u.USER_MOTHER_NAME,\r\n"
        		+ "       u.USER_NID,\r\n"
        		+ "       u.USER_PERMANENT_ADDRESS,\r\n"
        		+ "       u.USER_PRESENT_ADDRESS,\r\n"
        		+ "       u.USER_SELLING_AREA,\r\n"
        		+ "       u.USER_LICENSE_NO,\r\n"
        		+ "       u.USER_LICENSE_ISSUE_DATE,\r\n"
        		+ "       u.USER_BANK_SOLVENCY,\r\n"
        		+ "       u.USER_STATUS\r\n"
        		+ "  FROM user_master u where u.user_id = '" + userId +"' ";
        System.out.println("Sql query for getAllUserInfoByUserId: " + sqlQuery);
        Connection connection = jdbcTemplateObject.getDataSource().getConnection();
        PreparedStatement s=connection.prepareStatement(sqlQuery);      //creating statement
        ResultSet rs=s.executeQuery();   //executing statement
        
        System.out.println(rs.getFetchSize());
        if(rs.getFetchSize()>0)
        { while(rs.next()) {
            userMasterVo.setUserId(rs.getString(1));
            userMasterVo.setUserName(rs.getString(2));
            userMasterVo.setUserType(rs.getString(3));
            userMasterVo.setUserMobile(rs.getString(4));
            userMasterVo.setUserEmail(rs.getString(5));
            userMasterVo.setUserFathersName(rs.getString(6));
            userMasterVo.setUserMothersName(rs.getString(7));
            userMasterVo.setUserNid(rs.getString(8));
            userMasterVo.setUserPermanentAddress(rs.getString(9));
            userMasterVo.setUserPresentAddress(rs.getString(10));
            userMasterVo.setUserSellingArea(rs.getString(11));
            userMasterVo.setUserLicenseNo(rs.getString(12));
            userMasterVo.setUserLicenseIssueDate(rs.getDate(13));
            userMasterVo.setUserBankSolvency(rs.getString(14));
            userMasterVo.setUserStatus(rs.getString(15));
        }
        }
        connection.close();
        return userMasterVo;
    }

    
    

}
